<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bussiness extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/bussiness
	 *	- or -
	 * 		http://example.com/index.php/bussiness/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/bussiness/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('admin_master_model');

		$this->load->database();

		$this->load->library('session');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('email');

		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('form');

	}

	public function summary()
	{
		$this->admin_model->admin_auth();
        // $this->admin_model->menu_secure_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$data['cont_head_h1']   = 'Paket Bisnis<small>halaman paket bisnis</small>';
        $data['breadcrumb']     = '<li><a href="'.site_url().'/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
        $data['breadcrumb']     .= '<li><a>Summary</a></li>';

		$user_email			= $this->session->email;
		if (isset($_GET['usr']))
		{
			$data['user_info']	= $this->admin_master_model->get_row('partner','p_id',$_GET['usr']);
			$user_email = $data['user_info']['username'];
		}

		$data['user_auth']	= $this->admin_master_model->get_row('user','username',$user_email);
		$data['user_perm']	= $this->admin_master_model->get_row('user_permission','id_perm',$data['user_auth']['permission']);
		$data['user_info']	= $this->admin_master_model->get_row('partner','username',$user_email);
		$data['provinces']	= $this->admin_master_model->get_row('locat_provinces','id',$data['user_info']['lp_id']);
		$data['regencies']	= $this->admin_master_model->get_row('locat_regencies','id',$data['user_info']['lr_id']);
		$data['membership']	= $this->admin_master_model->get_row('el_membership','em_id',$data['user_info']['em_id']);

		// order
		$data['partner_order']	= $this->admin_master_model->get_result_where('partner_order','p_id',$data['user_info']['p_id']);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/partner/summary', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function package()
	{
		$this->admin_model->admin_auth();
        $this->admin_model->menu_secure_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		if (isset($_POST['btn_package']))
		{
			foreach ($_POST['ec_field'] as $key => $value)
			{
				$ec_field_arr[] = $key.":".$value;
			}
			$ec_field = implode(',',$ec_field_arr);

			$dataPackage = array(
				'ep_id'	=> $this->input->post('ep_id'),
				'em_id' => $this->input->post('em_id'),
				'ebp_code'	=> $this->input->post('ebp_code'),
				'ec_field'	=> $ec_field,
				'ebp_price'	=> $this->input->post('ebp_price')
			);

			$this->admin_master_model->db_insert('el_bussiness_package', $dataPackage);

			$data['notif_package'] = 'Data '.$this->input->post('ebp_code').' berhasil di masukkan.';
		}

		if (isset($_POST['btn_package_edit']))
		{
			foreach ($_POST['ec_field'] as $key => $value)
			{
				$ec_field_arr[] = $key.":".$value;
			}
			$ec_field = implode(',',$ec_field_arr);

			$dataPackage = array(
				'ep_id'	=> $this->input->post('ep_id'),
				'em_id' => $this->input->post('em_id'),
				'ebp_code'	=> $this->input->post('ebp_code'),
				'ec_field'	=> $ec_field,
				'ebp_price'	=> $this->input->post('ebp_price')
			);

			$this->admin_master_model->db_update('el_bussiness_package','ebp_id',$this->input->post('ebp_id'),$dataPackage);

			$data['notif_package'] = 'Data '.$this->input->post('ebp_code').' berhasil di masukkan.';
		}

        $data['el_product']		= $this->admin_master_model->get_result('el_product');
		$data['el_membership']	= $this->admin_master_model->get_result('el_membership');

        $data['cont_head_h1']   = 'Paket Bisnis<small>halaman paket bisnis</small>';
        $data['breadcrumb']     = '<li><a href="'.site_url().'/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
        $data['breadcrumb']     .= '<li><a>Paket Bisnis</a></li>';

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/package/package', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function partner()
	{
		// $this->admin_model->admin_auth();
        // $this->admin_model->menu_secure_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		if (isset($_GET['check_postalcode']))
		{
			$wherePartner = array(
				'p_postalcode'	=> $this->input->get('p_postalcode')
			);
			$data['check_parent'] = $this->admin_master_model->get_result_where_array('partner',$wherePartner);
		}

		if (isset($_POST['partner_btn']))
		{
			$data['cek_user']	= $this->admin_master_model->get_row('user','username',$this->input->post('username'));

			if ($data['cek_user'])
			{
				$data['notif_partner'] = 'Email <b>'.$this->input->post('username').'</b> sudah digunakan. Silahkan menggunakan email yang lain.';
			}
			else
			{
				$dataPartner = array(
					'p_id_parent'	=> $this->input->post('p_id'),
					'em_id'		=> $this->input->post('em_id'),
					'p_name'	=> $this->input->post('p_name'),
					'username'	=> $this->input->post('username'),
					'p_phone'	=> $this->input->post('p_phone'),
					'p_address'	=> $this->input->post('p_address'),
					'lp_id'	=> $this->input->post('lp_id'),
					'lr_id'	=> $this->input->post('lr_id'),
					'p_postalcode'	=> $this->input->post('p_postalcode'),
					'p_joindate'	=> date('Y-m-d H:i:s')
				);
				$this->admin_master_model->db_insert('partner', $dataPartner);
				
				$dataUser = array(
					'nama'		=> $this->input->post('p_name'),
					'username'	=> $this->input->post('username'),
					'password'	=> md5($this->input->post('password')),
					'permission'=> 5
				);
				$this->admin_master_model->db_insert('user', $dataUser);
	
				$data['notif_partner'] = 'Data <b>'.$this->input->post('p_name').'</b> berhasil di masukkan.';
			}
		}

		$data['el_membership']	= $this->admin_master_model->get_result('el_membership');
		$data['partner']		= $this->admin_master_model->get_result('partner');
		$data['locat_provinces']= $this->admin_master_model->get_result('locat_provinces');
		$data['locat_regencies']= $this->admin_master_model->get_result('locat_regencies');

		$data['cont_head_h1']   = 'Partner<small>halaman data partner dan formulir registrasi</small>';
        $data['breadcrumb']     = '<li><a href="'.site_url().'/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
        $data['breadcrumb']     .= '<li><a>Partner</a></li>';

		$this->load->view('themeAdmin/header');
		if ($this->session->is_admin == 1)
		{
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
		}
		$this->load->view('modul/partner/partner', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function order()
	{
		$this->admin_model->admin_auth();
        $this->admin_model->menu_secure_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		if (isset($_POST['order_btn']))
		{
			$p_id	= $this->input->post('p_id');
			$ebp_id	= $this->input->post('ebp_id');

			$data['partner']    			= $this->admin_master_model->get_row('partner','p_id',$p_id);
			$data['el_bussiness_package']	= $this->admin_master_model->get_row('el_bussiness_package','ebp_id',$ebp_id);
			$data['el_product']				= $this->admin_master_model->get_row('el_product','ep_id',$data['el_bussiness_package']['ep_id']);

			$dataPartnerOrder = array(
				'p_id'		=> $p_id,
				'ep_id'		=> $data['el_bussiness_package']['ep_id'],
				'ep_name'	=> $data['el_product']['ep_name'],
				'ebp_id'	=> $ebp_id,
				'ebp_code'	=> $data['el_bussiness_package']['ebp_code'],
				'ec_field'	=> $data['el_bussiness_package']['ec_field'],
				'ebp_price'	=> $data['el_bussiness_package']['ebp_price'],
				'po_createdate'	=> date('Y-m-d H:i:s')
			);
			$this->admin_master_model->db_insert('partner_order', $dataPartnerOrder);
			$data['partner_order_max'] = $this->admin_master_model->get_max('partner_order','po_id');

			$ec_field = explode(',', $data['el_bussiness_package']['ec_field']);

			foreach ($ec_field as $ec_field_key => $ec_field_value) {
				$ec_field_value = explode(':', $ec_field_value);
				$data['el_customfield'] = $this->admin_master_model->get_row('el_customfield','ec_id',$ec_field_value[0]);
				$dataPartnerOrderCalc = array(
					'po_id'		=> $data['partner_order_max']['po_id'],
					'ec_id'		=> $ec_field_value[0],
					'ec_declar'	=> $data['el_customfield']['ec_declar'],
					'ec_declar_cat'	=> $data['el_customfield']['ec_declar_cat'],
					'poc_number'	=> $ec_field_value[1]
				);
				$this->admin_master_model->db_insert('partner_order_calc', $dataPartnerOrderCalc);
			}

			$data['notif_order'] = 'Order atas nama <strong>'.$data['partner']['p_name'].'</strong> berhasil dimasukkan.';
		}

		$data['el_bussiness_package']	= $this->admin_master_model->get_result('el_bussiness_package');
		$data['partner']				= $this->admin_master_model->get_result('partner');
		$data['partner_order']			= $this->admin_master_model->get_result_order('partner_order','po_createdate','desc');
		$data['s_partner']				= '';
		if ($this->session->userdata('permission') == 5)
		{
			$data['partner']			= $this->admin_master_model->get_result_where('partner','username',$this->session->userdata('email'));
			$orderWhere = array('p_id' => $data['partner'][0]['p_id']);
			$data['partner_order']		= $this->admin_master_model->get_result_where_order_array('partner_order',$orderWhere,'po_createdate','desc');
			$data['s_partner']			= 'selected';
		}

		$data['cont_head_h1']   = 'Order<small>halaman data order dan formulir order</small>';

		$act = $this->uri->segment(3);
		if ($act == 'history_payment')
		{
			$pocWhere = array(
				'po_id'			=> $this->uri->segment(4),
				'ec_declar_cat'	=> 'harga'
			);
			$data['partner_order_calc']	= $this->admin_master_model->get_result_where_array('partner_order_calc',$pocWhere);

			$data['breadcrumb']     = '<li><a href="'.site_url().'/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
			$data['breadcrumb']     .= '<li><a href="'.site_url().'/bussiness/order">Order</a></li>';
			$data['breadcrumb']     .= '<li><a>Payment History</a></li>';

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/partner/partner_order_history_payment', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
		else
		{
			$data['breadcrumb']     = '<li><a href="'.site_url().'/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
			$data['breadcrumb']     .= '<li>Order</li>';

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/partner/partner_order', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
	}

	public function select_location()
	{
		$modul	= $this->input->post('modul');
		$id		= $this->input->post('id');

		// $modul	='kabupaten';
		// $id		= 11;

		if($modul == 'kabupaten')
		{
			echo $this->admin_master_model->get_select_param_order('locat_regencies','province_id',$id,'id','name','name','asc');
		}
	}
}//EOF class Bussiness extends CI_Controller

/* End of file Bussiness.php */
/* Location: ./application/controllers/Bussiness.php */
