<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('content_model');
		$this->load->model('content_admin_model');
		$this->load->model('admin_master_model');

		$this->load->helper('url');
	}

	public function index()
	{
		$data['slideshow']	= $this->content_admin_model->db_get_slideshow();

		$promoWhere = array('content_type' => 'promo');
		$data['promo']		= $this->admin_master_model->get_result_where_order_limit_array('content',$promoWhere,2,0,'id','desc');

		$videoWhere = array('content_type' => 'video');
		$data['video']		= $this->admin_master_model->get_result_where_order_limit_array('content',$videoWhere,3,0,'id','desc');

		$artikelWhere = array('content_type' => 'artikel');
		$data['latestArtikel']	= $this->admin_master_model->get_result_where_order_limit_array('content',$artikelWhere,1,0,'id','desc');

		$newsWhere = array('content_type' => 'news');
		$data['latestNews']		= $this->admin_master_model->get_result_where_order_limit_array('content',$newsWhere,1,0,'id','desc');

		$this->load->view('themeElcure/header');
		$this->load->view('themeElcure/header_nav');
		$this->load->view('themeElcure/index', $data);
		$this->load->view('themeElcure/footer');
	}

	public function plan()
	{
		$data[] = '';
		
		$id = $this->uri->segment(3);

		$this->load->view('themeTrans/header');
		$this->load->view('themeTrans/header_nav');
		$this->load->view('themeTrans/plan', $data);
		$this->load->view('themeTrans/footer_index');
		$this->load->view('themeTrans/footer');
	}
}
