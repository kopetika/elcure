<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/content_admin
	 *	- or -
	 * 		http://example.com/index.php/content_admin/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/admin/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('content_admin_model');
		$this->load->model('content_model');

		$this->load->database();

		$this->load->library('session');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('email');

		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('form');

	}

	/* Content Manager */
	public function content()
	{
		$this->admin_model->admin_auth();
		//$this->admin_model->menu_secure_auth();

		if (isset($_POST['btn_delete']))
		{
			$content_id = $_POST['id'];
			$this->content_admin_model->content_delete($content_id);
		}

		if (isset($_POST['btn_content_project']))
		{
			$this->content_admin_model->db_content_add_project_insert();
		}

		if (isset($_POST['btn_content_project_edit']))
		{
			$id = $this->input->post('id');
			$this->content_admin_model->db_content_project_update($id);
		}

		// untuk menyembunyikan elemen content_type
		$data['css_hidden'] = '';
		if ($this->uri->segment(3) == 'manual')
		{
			$data['css_hidden'] = 'style="display:none;"';
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

			//pengaturan pagination
			$config['base_url']		= site_url().'/content_admin/content/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
			$config['total_rows']	= $this->content_admin_model->db_content_num_rows();
			$config['per_page']		= $per_page = 10;
			$config['uri_segment']	= 5;
			//pengaturan tampilan pagination
			$config['full_tag_open'] = '<ul class="pagination no-margin-top">';
			$config['full_tag_close'] = '</ul>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= '&raquo;';
			$config['prev_link']	= '&laquo;';

			$this->pagination->initialize($config);

			$data['paging'] = $this->pagination->create_links();

			$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

			$data['content_list']=$this->content_admin_model->db_content_par($per_page, $page, 0);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function content_add()
	{
		$this->admin_model->admin_auth();

		if (isset($_POST['btn_content_debug']))
		{
			$this->content_admin_model->db_content_add_insert();

			$data['notif_content_add'] = "Data berhasil di input.";
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$content_type	= $this->uri->segment(3);
		$data['content_cat']	= $this->content_admin_model->content_kategori(0,$content_type);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content_add', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');

		//$this->load->view('wysiwyg');
	}

	public function content_edit()
	{
		$this->admin_model->admin_auth();

		if (isset($_POST['btn_content_edit']))
		{
			$id = $this->input->post('id');
			$this->content_admin_model->db_content_update($id);

			$data['notif_content_edit'] = "Data berhasil di ubah.";
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$id = $this->uri->segment(5);
		$data['content_edit']=$this->content_admin_model->db_content_edit($id);

		$content_type	= $this->uri->segment(3);
		$data['content_cat']	= $this->content_admin_model->content_kategori(0,$content_type);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content_edit', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');

		//$this->load->view('wysiwyg');
	}

	public function content_update()
	{
		$this->admin_model->admin_auth();

		$data['content_edit']=$this->content_admin_model->db_content_update();

		header('location:'.site_url().'/admin/content_edit/'.$this->input->post('id'));
	}

	public function content_foto()
	{
		$this->admin_model->admin_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$id = $this->uri->segment(5);

		$data['notif_photo_upload'] = '';
		if (isset($_POST['field_val']))
		{
			$config['upload_path'] = 'user_upload/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			//$config['max_size']	= '2048';
			$config['max_width']  = '2000';
			$config['max_height']  = '1500';

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload())
			{
				$data['error'] = $this->upload->display_errors();
				$data['notif_photo_upload'] = '2';
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$photo_produk	= $data['upload_data']['raw_name'].$data['upload_data']['file_ext'];
				$field_val		= $this->input->post('field_val');

				$this->content_admin_model->set_content_img($id,$photo_produk,$field_val);

				$data['notif_photo_upload'] = '1';
			}
		}

		if (isset($_POST['btn_delete_img']))
		{
			$photo_produk	= $this->input->post('foto');
			$field_val_del	= $this->input->post('field_val_del');

			$this->content_admin_model->set_content_img($id,$photo_produk,$field_val_del);
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$data['cek_content'] = $this->content_admin_model->db_content_edit($id);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content_foto', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function content_category()
	{
		$this->admin_model->admin_auth();

		if (isset($_POST['btn_catadd']))
		{
			$par_id		= $this->input->post('par_id');
			$name		= $this->input->post('cat_name');
			$cat_desc	= $this->input->post('cat_desc');
			$content_type = $this->uri->segment(3);

			$this->content_admin_model->add_content_kategori($par_id,$name,$cat_desc,$content_type);
		}

		if (isset($_POST['btn_catedit']))
		{
			$id			= $this->input->post('id');
			$name		= $this->input->post('cat_name');
			$cat_desc	= $this->input->post('cat_desc');

			$this->content_admin_model->update_content_kategori($id,$name,$cat_desc);
		}

		if (isset($_POST['btn_catdel']))
		{
			$id = $this->input->post('id');

			$this->content_admin_model->delete_content_kategori($id);
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$content_type	= $this->uri->segment(3);
		$data['content_cat']	= $this->content_admin_model->content_kategori(0,$content_type);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content_cat', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function category_foto()
	{
		$this->admin_model->admin_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$id = $this->uri->segment(5);

		$data['notif_photo_upload'] = '';
		if (isset($_POST['field_val']))
		{
			$config['upload_path'] = 'user_upload/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			//$config['max_size']	= '2048';
			$config['max_width']  = '2000';
			$config['max_height']  = '1500';

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload())
			{
				$data['error'] = $this->upload->display_errors();
				$data['notif_photo_upload'] = '2';
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$photo_produk	= $data['upload_data']['raw_name'].$data['upload_data']['file_ext'];
				$field_val		= $this->input->post('field_val');

				$this->content_admin_model->set_category_img($id,$photo_produk,$field_val);

				$data['notif_photo_upload'] = '1';
			}
		}

		if (isset($_POST['btn_delete_img']))
		{
			$photo_produk	= $this->input->post('foto');
			$field_val_del	= $this->input->post('field_val_del');

			$this->content_admin_model->set_category_img($id,$photo_produk,$field_val_del);
		}

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$data['cek_content'] = $this->content_admin_model->content_kategori_single($id);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('modul/content/content_cat_foto', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	public function upload_content_exec()
	{
		$this->admin_model->admin_auth();

		$config['upload_path'] = 'user_uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '2000';
		$config['max_height']  = '2000';

		$this->load->library('upload', $config);

		if ( !$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors('<div class="error">', '</div>'));

			$this->load->view('themeAdmin/header');
			$this->load->view('modul/adminArea/upload_content_form', $error);
			$this->load->view('themeAdmin/footer');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('themeAdmin/header');
			$this->load->view('modul/adminArea/upload_content_success', $data);
			$this->load->view('themeAdmin/footer');
		}
	}
	/* Content Manager */

}//EOF class Content_admin extends CI_Controller

/* End of file Content_admin.php */
/* Location: ./application/controllers/Content_admin.php */
