<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/content
	 *	- or -
	 * 		http://example.com/index.php/content/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/content/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('content_model');
		$this->load->model('content_admin_model');

		$this->load->library('cart');

		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['content'] = $this->content_model->db_get_content();

		//$this->load->view('themeDefault/header');
		//$this->load->view('themeDefault/index', $data);
		//$this->load->view('themeDefault/footer');
	}

	public function view($slug)
	{
		$data['content'] = $this->content_model->db_get_content($slug);

		if (empty($data['content']))
		{
			show_404();
		}

		$this->load->view('themeElcure/header', $data);
		$this->load->view('themeElcure/header_nav', $data);
		$this->load->view('modul/content/view', $data);
		$this->load->view('themeElcure/footer');
	}

	public function mod($type)
	{
		$mod_act = $this->uri->segment(4);
		if ($mod_act == 'cat')
		{
			$catslug = $this->uri->segment(5);
			$data['cat_slug'] = $this->content_model->get_cat_slug_single($catslug);
			$data['cat_sub']  = $this->content_admin_model->content_kategori($data['cat_slug']['id'],$this->uri->segment(3));

			$data['mod_type'] = $this->content_model->get_content_type_cat($type,$data['cat_slug']['id']);

			$this->load->view('themeTrans/header', $data);
			$this->load->view('themeTrans/header_nav', $data);
			$this->load->view('modul/content/cat', $data);
			$this->load->view('themeTrans/footer');
		}
		else
		{
			$catslug = $this->uri->segment(4);

			if (isset($catslug))
			{
				$data['cat_slug'] = $this->content_model->get_cat_slug_single($catslug);
				$data['mod_type'] = $this->content_model->get_content_type_cat($type,$data['cat_slug']['id']);
			}
			else
			{
				$data['mod_type'] = $this->content_model->get_content_type($type);
			}

			$this->load->view('themeElcure/header', $data);
			$this->load->view('themeElcure/header_nav', $data);
			$this->load->view('modul/content/mod', $data);
			$this->load->view('themeElcure/footer');
		}
	}

	public function galery()
	{
		$data['galery'] = $this->content_model->get_content_type('galery');

		$data['page_title'] = 'Gallery';

		$this->load->view('themeElcure/header', $data);
		$this->load->view('themeElcure/header_nav', $data);
		$this->load->view('modul/content/mod_galery', $data);
		$this->load->view('themeElcure/footer');
	}

	public function promo()
	{
		$data['galery'] = $this->content_model->get_content_type('promo');

		$data['page_title'] = 'Promo';

		$this->load->view('themeElcure/header', $data);
		$this->load->view('themeElcure/header_nav', $data);
		$this->load->view('modul/content/mod_galery', $data);
		$this->load->view('themeElcure/footer');
	}

	public function video()
	{
		$data['video'] = $this->content_model->get_content_type('video');

		$this->load->view('themeElcure/header', $data);
		$this->load->view('themeElcure/header_nav', $data);
		$this->load->view('modul/content/mod_video', $data);
		$this->load->view('themeElcure/footer');
	}
}

/* End of file content.php */
/* Location: ./application/controllers/content.php */
