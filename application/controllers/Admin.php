<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/admin
	 *	- or -
	 * 		http://example.com/index.php/admin/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/admin/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');

		$this->load->database();

		$this->load->library('session');
		$this->load->library('table');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('email');

		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('form');

	}

	public function login()
	{
		$data['sukses'] = "0";
		if (isset($_POST['login_btn']))
		{
			$username	= $this->input->post('username');
			$password	= $this->input->post('password');

			$data['auth_login'] = $this->admin_model->login($username,$password);

			if (empty($data['auth_login']))
			{
				$this->session->sess_destroy();
				$data['sukses'] = "1";
			}
			else
			{
				$data['sukses'] = "2";
			}
		}

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/login', $data);
		$this->load->view('themeAdmin/footer');
	}

	public function index()
	{
		$this->admin_model->admin_auth();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$this->load->view('themeAdmin/header');
		$this->load->view('themeAdmin/header_nav', $data);
		$this->load->view('themeAdmin/sidebar_left', $data);
		$this->load->view('themeAdmin/index_admin', $data);
		$this->load->view('themeAdmin/index_footer');
		$this->load->view('themeAdmin/footer');
	}

	//START: Controler for User Management System
	public function user()
	{
		$this->admin_model->admin_auth();
		$this->admin_model->admin_otorise();

		//[Harus selalu ada di setiap fungsi]:digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$user_act = $this->uri->segment(3);

		if ($user_act == 'edit')
		{
			$data['sukses'] = "0";
			if (isset($_POST['user_edit_btn']))
			{
				$username = $this->uri->segment(4);
				$nama	= $this->input->post('nama');
				$email	= $this->input->post('email');
				$telpon	= $this->input->post('telpon');

				$this->admin_model->db_member_update($username,$nama,$email,$telpon);

				$data['sukses'] = "1";
			}

			$username = $this->uri->segment(4);
			$data['user'] = $this->admin_model->get_member_update($username);

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/user_edit', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
		elseif ($user_act == 'edit_password')
		{
			if (isset($_POST['user_editpass_btn']))
			{
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');

				if ($this->form_validation->run() == FALSE)
				{
					$data['sukses'] = "0";

					$this->load->view('themeAdmin/header');
					$this->load->view('themeAdmin/header_nav', $data);
					$this->load->view('themeAdmin/sidebar_left', $data);
					$this->load->view('modul/_usermanager/user_edit_password', $data);
					$this->load->view('themeAdmin/index_footer');
					$this->load->view('themeAdmin/footer');
				}
				else
				{
					$username = $this->uri->segment(4);
					$password = $this->input->post('password');

					$this->admin_model->db_update_pass_dev($username,$password);

					$data['sukses'] = "1";

					$this->load->view('themeAdmin/header');
					$this->load->view('themeAdmin/header_nav', $data);
					$this->load->view('themeAdmin/sidebar_left', $data);
					$this->load->view('modul/_usermanager/user_edit_password', $data);
					$this->load->view('themeAdmin/index_footer');
					$this->load->view('themeAdmin/footer');
				}
			}
			else
			{
				$data['sukses'] = "0";

				$this->load->view('themeAdmin/header');
				$this->load->view('themeAdmin/header_nav', $data);
				$this->load->view('themeAdmin/sidebar_left', $data);
				$this->load->view('modul/_usermanager/user_edit_password', $data);
				$this->load->view('themeAdmin/index_footer');
				$this->load->view('themeAdmin/footer');
			}
		}
		elseif ($user_act == 'delete')
		{
			$data['sukses'] = "0";
			if (isset($_POST['user_delete_btn']))
			{
				$username	= $this->input->post('username');

				$this->admin_model->db_delete_user($username);

				$data['sukses'] = "1";
			}

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/user_delete', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
		else
		{
			if (isset($_POST['user_add_btn']))
			{
				$nama	= $this->input->post('nama');
				$username	= $this->input->post('username');
				$password	= $this->input->post('password');
				$permission	= $this->input->post('permission');

				$this->admin_model->db_add_user($username,$password,$permission);
			}

			//pengaturan pagination
				$config['base_url']		= site_url().'/admin/user';
				$config['total_rows']	= $this->admin_model->get_member_list_num_rows_admin();
				$config['per_page']		= $per_page = 10;
				$config['uri_segment']	= 3;
			//pengaturan tampilan pagination
				$config['full_tag_open']	= '<ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_clode']	= '</ul>';
				$config['next_tag_open']	= '<li>';
				$config['next_tag_close']	= '</li>';
				$config['prev_tag_open']	= '<li>';
				$config['prev_tag_close']	= '</li>';
				$config['first_tag_open']	= '<li>';
				$config['last_tag_close']	= '</li>';
				$config['cur_tag_open']		= '<li class="active"><a>';
				$config['cur_tag_close']	= '</a></li>';
				$config['num_tag_open'] 	= '<li>';
				$config['num_tag_close']	= '</li>';
				$config['next_link']	= '&raquo;';
				$config['prev_link']	= '&laquo;';
				$config['first_link']	= 'First';
				$config['last_link']	= 'Last';

				$this->pagination->initialize($config);

				$data['paging'] = $this->pagination->create_links();

				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

				$data['admin_list'] = $this->admin_model->get_member_list_admin($per_page, $page);

			$data['permission'] = $this->admin_model->get_permission();
			//$data['staf_admin'] = $this->admin_model->get_staf_data();

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/user_list', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
	}

	public function group()
	{
		$this->admin_model->admin_auth();
		$this->admin_model->admin_otorise();

		//[Harus selalu ada di setiap fungsi]:Digunakan untuk menampilkan menu
		$user_perm		= $this->session->permission;
		$data['menu']	= $this->admin_model->get_menu_permission($user_perm);
		$data['perm']	= $this->admin_model->get_group_name($user_perm);

		$group_act = $this->uri->segment(3);

		if ($group_act == 'menu')
		{
			if (isset($_POST['add_menu_btn']))
			{
				$user_perm	= $this->input->post('id_perm');
				$id_menu	= $this->input->post('id_menu');

				$this->admin_model->db_add_usermenu($user_perm,$id_menu);
			}

			$menu_act = $this->uri->segment(5);

			if ($menu_act == 'delete')
			{
				$data['notif_group_menu_delete'] = 0;
				if (isset($_POST['menu_group_delete_btn']))
				{
					$this->admin_model->db_delete_menu_group($this->uri->segment(6));

					$data['notif_group_menu_delete'] = 1;
				}

				$data['menu_group_delete'] = $this->admin_model->get_group_menu_single($this->uri->segment(6));

				$this->load->view('themeAdmin/header');
				$this->load->view('themeAdmin/header_nav', $data);
				$this->load->view('themeAdmin/sidebar_left', $data);
				$this->load->view('modul/_usermanager/group_menu_delete', $data);
				$this->load->view('themeAdmin/index_footer');
				$this->load->view('themeAdmin/footer');
			}
			else
			{
				$id_user_perm = $this->uri->segment(4);

				$data['group_name']	= $this->admin_model->get_group_name($id_user_perm);
				$data['group_menu']	= $this->admin_model->get_group_menu($id_user_perm);
				$data['menu_tg'] 	= $this->admin_model->get_menu();

				$this->load->view('themeAdmin/header');
				$this->load->view('themeAdmin/header_nav', $data);
				$this->load->view('themeAdmin/sidebar_left', $data);
				$this->load->view('modul/_usermanager/group_menu', $data);
				$this->load->view('themeAdmin/index_footer');
				$this->load->view('themeAdmin/footer');
			}
		}
		elseif ($group_act == 'edit')
		{
			$data['sukses'] = "0";
			if (isset($_POST['group_edit_btn']))
			{
				$id_perm	= $this->input->post('id');
				$name		= $this->input->post('nama');

				$this->admin_model->db_group_update($id_perm,$name);

				$data['sukses'] = "1";
			}

			$id_permit = $this->uri->segment(4);

			$data['group'] = $this->admin_model->get_group_update($id_permit);

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/group_edit', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
		elseif ($group_act == 'delete')
		{
			$data['sukses'] = "0";
			if (isset($_POST['group_delete_btn']))
			{
				$id_perm	= $this->input->post('id');

				$this->admin_model->db_delete_group($id_perm);

				$data['sukses'] = "1";
			}

			$id_permit = $this->uri->segment(4);

			$data['group'] = $this->admin_model->get_group_update($id_permit);

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/group_delete', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
		else
		{
			if (isset($_POST['group_add_btn']))
			{
				$name	= $this->input->post('name');

				$this->admin_model->db_add_group($name);
			}

			//pengaturan pagination
				$config['base_url']		= site_url().'/admin/group';
				$config['total_rows']	= $this->admin_model->get_member_group_num_rows_admin();
				$config['per_page']		= $per_page = 10;
				$config['uri_segment']	= 3;
			//pengaturan tampilan pagination
				$config['full_tag_open']	= '<ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_clode']	= '</ul>';
				$config['next_tag_open']	= '<li>';
				$config['next_tag_close']	= '</li>';
				$config['prev_tag_open']	= '<li>';
				$config['prev_tag_close']	= '</li>';
				$config['first_tag_open']	= '<li>';
				$config['last_tag_close']	= '</li>';
				$config['cur_tag_open']		= '<li class="active"><a>';
				$config['cur_tag_close']	= '</a></li>';
				$config['num_tag_open'] 	= '<li>';
				$config['num_tag_close']	= '</li>';
				$config['next_link']	= '&raquo;';
				$config['prev_link']	= '&laquo;';
				$config['first_link']	= 'First';
				$config['last_link']	= 'Last';

				$this->pagination->initialize($config);

				$data['paging'] = $this->pagination->create_links();

				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

				$data['group_list'] = $this->admin_model->get_member_group_admin($per_page, $page);

			$this->load->view('themeAdmin/header');
			$this->load->view('themeAdmin/header_nav', $data);
			$this->load->view('themeAdmin/sidebar_left', $data);
			$this->load->view('modul/_usermanager/group_list', $data);
			$this->load->view('themeAdmin/index_footer');
			$this->load->view('themeAdmin/footer');
		}
	}
	//END: Controler for User Management System

	public function logout()
	{
		$this->session->sess_destroy();
		header('location:'.site_url().'/admin/login');
	}

}//EOF class Admin extends CI_Controller

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
