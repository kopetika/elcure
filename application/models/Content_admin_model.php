<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_admin_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
	}

	/* Content Manager DB */
	public function content_kategori_cek_child($par_id)
	{
		$this->db->select('*');
		$this->db->from('content_kategori');
		$this->db->where('par_id', $par_id);

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function content_kategori_single($id)
	{
		$this->db->select('*');
		$this->db->from('content_kategori');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function content_kategori($par_id,$content_type)
	{
		$this->db->select('*');
		$this->db->from('content_kategori');
		$this->db->where('par_id', $par_id);
		$this->db->where('content_type', $content_type);
		$this->db->order_by('id', "asc");

		$query = $this->db->get();

		return $query->result_array();
	}

	public function add_content_kategori($par_id,$name,$cat_desc,$content_type)
	{
		$data = array(
				'par_id'		=> $par_id,
				'name'			=> $name,
				'slug'			=> url_title($name, 'dash', TRUE),
				'content_type'	=> $content_type,
				'cat_desc'		=> $cat_desc
			);

		return $this->db->insert('content_kategori', $data);
	}

	public function update_content_kategori($id,$name,$cat_desc)
	{
		$data = array(
				'name'	=> $name,
				'slug'	=> url_title($name, 'dash', TRUE),
				'cat_desc'	=> $cat_desc
			);

		$this->db->where('id', $id);
		return $this->db->update('content_kategori', $data);
	}

	public function delete_content_kategori($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('content_kategori');
	}

	public function db_content($limit, $start)
	{
		$type = $this->uri->segment(3);
		$pos = $this->uri->segment(4);

		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('content_type', $type);
		$this->db->where('content_pos', $pos);
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_par($limit, $start, $sub_content)
	{
		$type = $this->uri->segment(3);
		$pos = $this->uri->segment(4);

		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('sub_content', $sub_content);
		$this->db->where('content_type', $type);
		$this->db->where('content_pos', $pos);
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_par_nolimit($sub_content)
	{
		$type = $this->uri->segment(3);
		$pos = $this->uri->segment(4);

		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('sub_content', $sub_content);
		$this->db->where('content_type', $type);
		$this->db->where('content_pos', $pos);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_by($kolom,$value)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($kolom, $value);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_num_rows()
	{
		$type = $this->uri->segment(3);
		$pos = $this->uri->segment(4);

		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('content_type', $type);
		$this->db->where('content_pos', $pos);

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function db_content_slideshow()
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('content_type', 'page');
		$this->db->where('content_pos', 'slideshow');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_add_insert()
	{
		$data = array(
				'sub_content'	=> $this->input->post('sub_content'),
				'menu'		=> $this->input->post('menu'),
				'title'		=> $this->input->post('title'),
				'slug'		=> url_title($this->input->post('title'), 'dash', TRUE),
				'content'	=> $this->input->post('content'),
				'content_type'	=> $this->input->post('content_type'),
				'content_pos'	=> $this->input->post('content_pos'),
				'content_by'	=> $this->input->post('content_by')
            );

		$this->db->insert('content', $data);
	}

	public function db_content_edit($id)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function db_content_update($id)
	{
		$data = array(
				'sub_content'	=> $this->input->post('sub_content'),
				'menu'		=> $this->input->post('menu'),
				'title'		=> $this->input->post('title'),
				'slug'		=> url_title($this->input->post('title'), 'dash', TRUE),
				'content'	=> $this->input->post('content'),
				'img'		=> $this->input->post('img'),
				'content_type'	=> $this->input->post('content_type'),
				'content_pos'	=> $this->input->post('content_pos'),
				'content_by'	=> $this->input->post('content_by')
            );

		$this->db->where('id', $id);
		$this->db->update('content', $data);
	}

	public function db_content_add_project_insert()
	{
		$data = array(
				'sub_content'	=> $this->input->post('sub_content'),
				'menu'		=> $this->input->post('menu'),
				'title'		=> $this->input->post('title'),
				'slug'		=> url_title($this->input->post('title'), 'dash', TRUE),
				'content'	=> $this->input->post('content'),
				'content_sys'	=> $this->input->post('content_sis'),
				'content_tech'	=> $this->input->post('content_tech'),
				'content_prod'	=> $this->input->post('content_prod'),
				'content_type'	=> $this->input->post('content_type'),
				'content_pos'	=> $this->input->post('content_pos'),
				'content_by'	=> $this->input->post('content_by')
            );

		$this->db->insert('content', $data);
	}

	public function db_content_project_update($id)
	{
		$data = array(
				'sub_content'	=> $this->input->post('sub_content'),
				'menu'		=> $this->input->post('menu'),
				'title'		=> $this->input->post('title'),
				'slug'		=> url_title($this->input->post('title'), 'dash', TRUE),
				'content'	=> $this->input->post('content'),
				'content_sys'	=> $this->input->post('content_sis'),
				'content_tech'	=> $this->input->post('content_tech'),
				'content_prod'	=> $this->input->post('content_prod'),
				'img'		=> $this->input->post('img'),
				'content_type'	=> $this->input->post('content_type'),
				'content_pos'	=> $this->input->post('content_pos'),
				'content_by'	=> $this->input->post('content_by')
            );

		$this->db->where('id', $id);
		$this->db->update('content', $data);
	}

	public function content_delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('content');
	}

	public function set_content_img($id,$foto,$field_val)
	{
		$data = array(
			   $field_val => $foto
			);

		$this->db->where('id', $id);
		$this->db->update('content', $data);
	}

	public function set_category_img($id,$foto,$field_val)
	{
		$data = array(
			   $field_val => $foto
			);

		$this->db->where('id', $id);
		$this->db->update('content_kategori', $data);
	}

	public function db_get_slideshow()
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('content_type', 'slideshow');

		$query = $this->db->get();

		return $query->result_array();
	}
	/* Content Manager DB */

}//EOF class Admin_content_model extends CI_Model

/* End of file Admin_content_model.php */
/* Location: ./application/models/Admin_content_model.php */
