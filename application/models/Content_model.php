<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function db_get_content($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('content');
			return $query->result_array();
		}

		$query = $this->db->get_where('content', array('slug' => $slug));
		return $query->row_array();
	}

	/* Page */
	public function db_menu_page_top()
	{
		$query = $this->db->get_where('content', array('content_type' => 'page', 'content_pos' => 'top'));
		return $query->result_array();
	}

	public function db_solution()
	{
		$query = $this->db->get_where('content', array('content_type' => 'page', 'content_pos' => 'solution-for'));
		return $query->result_array();
	}

	public function db_success()
	{
		$query = $this->db->get_where('content', array('content_type' => 'page', 'content_pos' => 'success-story'));
		return $query->result_array();
	}
	/* Page */

	public function get_content_type($c_type)
	{
		$query = $this->db->get_where('content', array('content_type' => $c_type));
		return $query->result_array();
	}

	public function get_content_type_cat($c_type,$sub_content)
	{
		$this->db->order_by('id', 'asc');
		$query = $this->db->get_where('content', array('content_type' => $c_type, 'sub_content' => $sub_content));
		return $query->result_array();
	}

	public function get_content_param($param,$value)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($param, $value);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_content_param_limit($param,$value,$limit,$start)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($param, $value);
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'desc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_cat_slug_single($slug)
	{
		$this->db->select('*');
		$this->db->from('content_kategori');
		$this->db->where('slug', $slug);

		$query = $this->db->get();

		return $query->row_array();
	}

	//Slideshow
	public function get_content_num_rows($param,$value)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($param, $value);

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function db_content_f($param,$value)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($param, $value);
		$this->db->limit(1);
		$this->db->order_by("id", "asc");

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_content_n($param,$value,$limit)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where($param, $value);
		$this->db->limit($limit, 1);
		$this->db->order_by("id", "asc");

		$query = $this->db->get();

		return $query->result_array();
	}
	//Slideshow

	public function count_sub_menu($sub_content)
	{
		$this->db->select('*');
		$this->db->from('content');
		$this->db->where('sub_content', $sub_content);

		$query = $this->db->get();

		return $query->num_rows();
	}

}

/* End of file content_model.php */
/* Location: ./application/models/content_model.php */
