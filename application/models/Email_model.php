<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function notif_newmember($email,$password)
	{
		$htmlContent = '<h1>Informasi Member Baru</h1>';
		$htmlContent .= '<div>Anda telah terdaftar di '.base_url().'.</div>';
		//$htmlContent .= '<div>'.site_url().'/member/login/aktivasi/'.$email.'/'.$activation_code.'</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>Silahkan login ke dashbord Anda menggunakan informasi berikut:</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>url: '.base_url().'</div>';
		$htmlContent .= '<div>Login: '.$email.'</div>';
		$htmlContent .= '<div>Password: '.$password.'</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>Anda bisa mengganti password sesuai dengan keinginan.</div>';

		$this->load->library('email');

		//$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from('no-reply@ikanasstan.id','Ikanasstan');
		$this->email->subject('[Ikanasstan] Member Baru');
		$this->email->message($htmlContent);

		return $this->email->send();
	}

	public function notif_resetpass($email,$password)
	{
		$htmlContent = '<h1>Informasi Reset Password</h1>';
		$htmlContent .= '<div>Password Anda telah di reset. Silahkan login ke dashbord Anda menggunakan informasi berikut:</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>url: '.base_url().'</div>';
		$htmlContent .= '<div>Login: '.$email.'</div>';
		$htmlContent .= '<div>Password: '.$password.'</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>Anda bisa mengganti password sesuai dengan keinginan.</div>';

		$this->load->library('email');

		//$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from('no-reply@ikanasstan.id','Ikanasstan');
		$this->email->subject('[Ikanasstan] Reset Password');
		$this->email->message($htmlContent);

		return $this->email->send();
	}

	public function notif_resendcode($email,$activation_code)
	{
		$htmlContent = '<h1>Informasi Kode Aktifasi</h1>';
		$htmlContent .= '<div>Berikut link aktifasi akun Anda:</div>';
		$htmlContent .= '<div>'.site_url().'/member/login/aktivasi/'.$email.'/'.$activation_code.'</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>Anda bisa mengganti password sesuai dengan keinginan.</div>';

		$this->load->library('email');

		//$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from('no-reply@ikanasstan.id','Ikanasstan');
		$this->email->subject('[Ikanasstan] Resend Kode Aktifasi');
		$this->email->message($htmlContent);

		return $this->email->send();
	}

	public function notif_sendinvoice($email,$htmlContent)
	{
		$this->load->library('email');

		//$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from('no-reply@ikanasstan.id','Ikanasstan');
		$this->email->subject('[Ikanasstan] Invoice');
		$this->email->message($htmlContent);

		return $this->email->send();
	}

	public function notif_pembayaran($email,$trx_id)
	{
		$htmlContent = '<h1>Informasi Pembayaran</h1>';
		$htmlContent .= '<div>Terimakasih Anda telah melakukan pembayaran.</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>TRX ID: '.$trx_id.'</div>';
		$htmlContent .= '<div>Status: SUKSES</div>';
		$htmlContent .= '<div>&nbsp;</div>';
		$htmlContent .= '<div>Silahkan login untuk melihat lebih detail.</div>';
		$htmlContent .= '<div>&nbsp;</div>';

		$this->load->library('email');

		//$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from('no-reply@ikanasstan.id','Ikanasstan');
		$this->email->subject('[Ikanasstan] Informasi Pembayaran');
		$this->email->message($htmlContent);

		return $this->email->send();
	}
}//EOF class Email_model extends CI_Model

/* End of file Email_model.php */
/* Location: ./application/models/Email_model.php */
