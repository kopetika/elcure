<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_master_model extends CI_Model {

    public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function db_insert($table, $data)
	{
		$this->db->insert($table, $data);
	}
	
	public function db_update($table,$param,$value,$data)
    {
		$this->db->where($param, $value);
        $this->db->update($table, $data);
    }

    public function get_row($table,$param,$value)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($param, $value);

		$query = $this->db->get();

		return $query->row_array();
    }

    public function get_result($table)
    {
        $this->db->select('*');
		$this->db->from($table);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_order($table,$cloumnOrder,$ascdsc)
    {
        $this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($cloumnOrder, $ascdsc);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_where($table,$param,$value)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($param, $value);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_where_array($table,$where)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($where);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_where_order_array($table,$where,$cloumnOrder,$ascdsc)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($where);
		$this->db->order_by($cloumnOrder, $ascdsc);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_where_order_limit_array($table,$where,$limit,$start,$cloumnOrder,$ascdsc)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($where);
		$this->db->order_by($cloumnOrder, $ascdsc);
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
    }

    public function get_result_where_limit_array($table,$where,$limit,$start)
    {
        $this->db->select('*');
		$this->db->from($table);
        $this->db->where($where);
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
    }

	public function get_max($table,$column)
	{
		$this->db->select_max($column);
		$this->db->from($table);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_max_param($table,$column,$param,$value)
	{
		$this->db->select_max($column);
		$this->db->from($table);
		$this->db->where($param, $value);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_min($table,$column)
	{
		$this->db->select_min($column);
		$this->db->from($table);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_min_param($table,$column,$param,$value)
	{
		$this->db->select_min($column);
		$this->db->from($table);
		$this->db->where($param, $value);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_distinct_result($table,$column)
	{
		$this->db->DISTINCT();
		$this->db->select($column);
		$this->db->from($table);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_distinct_result_order($table,$column,$orderby,$ascdesc)
	{
		$this->db->DISTINCT();
		$this->db->select($column);
		$this->db->from($table);
		$this->db->order_by($orderby, $ascdesc);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_table_num($table,$param,$value)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($param, $value);

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function get_select_param_order($table,$param,$value,$optValue,$optDisplay,$optParam,$ordValue)
	{
		$subValue="<option value=''>--Select--</option>";

		$this->db->select('*');
		$this->db->from($table);
        $this->db->where($param, $value);
		$this->db->order_by($optParam,$ordValue);

		$query = $this->db->get();

		foreach ($query->result_array() as $data )
		{
			$subValue.= "<option value='$data[$optValue]'>$data[$optDisplay]</option>";
		}

		return $subValue;
    }

	public function delete_param($param,$value,$table)
	{
		$this->db->where($param, $value);
		$this->db->delete($table);
	}
}
