<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function all_auth()
	{
		if ($this->session->userdata('is_admin') != 1)
		{
			if ($this->session->userdata('is_member') != 1)
			{
				if ($this->session->userdata('is_member') == 1)
				{
					header('location:'.site_url().'/member/logout');
				}
				elseif ($this->session->userdata('is_admin') == 1)
				{
					header('location:'.site_url().'/admin/logout');
				}
				else
				{
					header('location:'.site_url().'/member/logout');
				}

			}
		}
	}

	public function admin_auth()
	{
		if ($this->session->userdata('is_admin') != 1)
		{
			header('location:'.site_url().'/admin/logout');
		}
	}

	public function admin_otorise()
	{
		if ($this->session->permission != 1)
		{
			header('location:'.site_url().'/admin/logout');
		}
	}

	public function menu_secure_auth()
	{
		$current_menu = '/'.$this->uri->segment(1).'/'.$this->uri->segment(2);
		//$current_menu = $this->get_current_url();

		$data['menu'] = $this->get_menu_permission($this->session->permission);

		$i = '0';
		foreach ($data['menu'] as $row_menu_pre)
		{
			$row_menu[$i] = $row_menu_pre['url'];
			$i++;
		}

		$allow = in_array($current_menu, $row_menu);

		if (!$allow)
		{
			header('location:'.site_url().'/admin/logout');
		}
	}

	public function get_current_url()
	{
		$url = false;

		// check whether this script is being run as a web page
		if (isset($_SERVER['SERVER_ADDR']))
		{
			$is_https   = isset($_SERVER['HTTPS']) &&
						  'on' == $_SERVER['HTTPS'];
			$protocol   = 'http' . ($is_https ? 's' : '');
			$host       = isset($_SERVER['HTTP_HOST'])
							? $_SERVER['HTTP_HOST']
							: $_SERVER['SERVER_ADDR'];
			$port       = $_SERVER['SERVER_PORT'];
			$path_query = $_SERVER['REQUEST_URI'];

			$url = sprintf('%s://%s%s%s',
				$protocol,
				$host,
				$is_https
					? (443 != $port ? ':' . $port : '')
					: ( 80 != $port ? ':' . $port : ''),
				$path_query
			);
		}

		return $url;
	}

	public function get_user_single($user_id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_id', $user_id);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function login($username,$password)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_menu_permission($user_perm)
	{
		$this->db->select('
			user_menu.id AS um_id,
			user_menu.id_user_perm AS id_user_perm,
			user_menu.id_menu AS id_menu,
			menu.id AS id,
			menu.orderby AS orderby,
			menu.title AS title,
			menu.url AS url,
			menu.url_param AS url_param,
			menu.right_container AS right_container
		');
		$this->db->from('user_menu');
		$this->db->join('menu', 'user_menu.id_menu = menu.id');
		$this->db->where('id_user_perm', $user_perm);
		$this->db->order_by('orderby', 'asc');

		$query = $this->db->get();

		return $query->result_array();
	}

	//START: Model/query for User Management System
	public function get_member_list_admin($limit, $start)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->order_by("user_id", "asc");
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_member_list_num_rows_admin()
	{

		$this->db->select('*');
		$this->db->from('user');

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function get_member_update($username)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $username);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function db_member_update($username,$nama,$email,$telpon)
	{
		$data = array(
			'nama'	=> $nama,
			'email'	=> $email,
			'telp'	=> $telpon
		);

		$this->db->where('username', $username);
		$this->db->update('user', $data);
	}

	public function db_update_pass_dev($username,$password)
	{
		$this->db->set('password', md5($password));
		$this->db->where('username', $username);
		$this->db->update('user');
	}

	public function get_permission()
	{
		$this->db->select('*');
		$this->db->from('user_permission');
		$this->db->order_by("id_perm", "asc");

		$query = $this->db->get();

		return $query->result_array();
	}

	public function db_add_user($username,$password,$permission)
	{
		$data = array(
				'username'	=> $username,
				'password'	=> md5($password),
				'permission'	=> $permission
		);

		$this->db->insert('user', $data);
	}

	public function db_update_status($nama_staf,$auth_status)
	{
		$data = array(
			'auth_status'	=> $auth_status
		);

		$this->db->where('staf_id', $nama_staf);
		$this->db->update('staf', $data);
	}

	public function db_delete_user($username)
	{
		$this->db->where('username', $username);
		$this->db->delete('user');
	}

	public function get_member_group_admin($limit, $start)
	{
		$this->db->select('*');
		$this->db->from('user_permission');
		$this->db->order_by("id_perm", "asc");
		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_member_group_num_rows_admin()
	{

		$this->db->select('*');
		$this->db->from('user_permission');

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function db_add_group($name)
	{
		$data = array(
				'name'	=> $name
		);

		$this->db->insert('user_permission', $data);
	}

	public function get_group_update($id_permit)
	{
		$this->db->select('*');
		$this->db->from('user_permission');
		$this->db->where('id_perm', $id_permit);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function db_group_update($id_perm,$name)
	{
		$data = array(
			'name'	=> $name
		);

		$this->db->where('id_perm', $id_perm);
		$this->db->update('user_permission', $data);
	}

	public function get_group_menu($id_user_perm)
	{
		$this->db->select('*');
		$this->db->from('user_menu');
		$this->db->where('id_user_perm', $id_user_perm);
		$this->db->join('menu', 'user_menu.id_menu = menu.id');
		$this->db->order_by("orderby", "asc");

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_group_menu_single($id_menu)
	{
		$this->db->select('*');
		$this->db->from('user_menu');
		$this->db->where('id_menu', $id_menu);
		$this->db->join('menu', 'user_menu.id_menu = menu.id');
		$this->db->order_by("orderby", "asc");

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_group_name($id_user_perm)
	{
		$this->db->select('*');
		$this->db->from('user_permission');
		$this->db->where('id_perm', $id_user_perm);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function db_delete_group($id_perm)
	{
		$this->db->where('id_perm', $id_perm);
		$this->db->delete('user_permission');
	}

	public function db_delete_menu_group($id_menu)
	{
		$this->db->where('id_menu', $id_menu);
		$this->db->delete('user_menu');
	}

	public function db_add_usermenu($user_perm,$id_menu)
	{
		$data = array(
				'id_user_perm'	=> $user_perm,
				'id_menu'		=> $id_menu
		);

		$this->db->insert('user_menu', $data);
	}
	//END: Model/query for User Management System

	public function get_menu()
	{
		/*
		Fungsi ini digunakan pada:
		- Controler:admin:group:menu
		*/
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->order_by("orderby", "asc");

		$query = $this->db->get();

		return $query->result_array();
	}

	//START: Master data
	public function get_master_selected($id,$table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_locat_provinces()
	{
		$this->db->select('*');
		$this->db->from('locat_provinces');
		$this->db->order_by('name', 'asc');

		$query = $this->db->get();

		return $query->result_array();
	}

}//EOF class Admin_model extends CI_Model

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */
