		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?=$cont_head_h1?></h1>
				<ol class="breadcrumb">
					<?=$breadcrumb?>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <?php
                    if (isset($_POST['order_btn_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";
                    }
                ?>
                <?php if (isset($notif_order)): ?>
                    <div class="row">
						<div class="col-md-12">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-info"></i> Notification</h4>
								<?=$notif_order?>
							</div>
						</div>
					</div>
                <?php endif; ?>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Partner Order</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table id="example1" class="table">
                            <thead>
                                <tr>
									<th></th>
                                    <th>Nama</th>
                                    <th>Telephone</th>
                                    <th>Paket Bisnis</th>
                                    <th>Keagenan</th>
                                    <th>Kode ID</th>
                                    <th>Harga</th>
                                    <th>Tanggal Order</th>
									<th>Alamat</th>
									<th>Kota</th>
									<th>Provinsi</th>
									<th></th>
                                </tr>
                            </thead>
                            <tbody>
								<?php $ourut = 1; ?>
                                <?php foreach ($partner_order as $partner_order_list): ?>
                                <?php $data['partner']              = $this->admin_master_model->get_row('partner','p_id',$partner_order_list['p_id']); ?>
                                <?php $data['el_bussiness_package'] = $this->admin_master_model->get_row('el_bussiness_package','ebp_id',$partner_order_list['ebp_id']); ?>
                                <?php $data['el_membership']        = $this->admin_master_model->get_row('el_membership','em_id',$data['el_bussiness_package']['em_id']); ?>
                                <?php $data['locat_provinces']  	= $this->admin_master_model->get_row('locat_provinces','id',$data['partner']['lp_id']); ?>
                                <?php $data['locat_regencies']  	= $this->admin_master_model->get_row('locat_regencies','id',$data['partner']['lr_id']); ?>
                                    <tr>
										<td><?=$ourut?></td>
                                        <td><?=$data['partner']['p_name']?></td>
                                        <td><?=$data['partner']['p_phone']?></td>
                                        <td><?=$partner_order_list['ep_name']?></td>
                                        <td><?=$data['el_membership']['em_name']?></td>
                                        <td><?=$partner_order_list['ebp_code']?></td>
                                        <td><?=number_format($partner_order_list['ebp_price'],0,",",".")?></td>
                                        <td><?=date('d F Y', strtotime($partner_order_list['po_createdate']))?>, pukul <?=date('H:i:s', strtotime($partner_order_list['po_createdate']))?></td>
										<td><?=$data['partner']['p_address']?></td>
										<td><?=$data['locat_regencies']['name']?></td>
										<td><?=$data['locat_provinces']['name']?></td>
										<td>
											<?php if ($this->session->userdata('permission') != 5): ?>
												<a href="<?=current_url()?>/history_payment/<?=$partner_order_list['po_id']?>" class="btn btn-info btn-xs">History Pembayaran</a>
												<a href="<?=current_url()?>/history_botol/<?=$partner_order_list['po_id']?>" class="btn btn-primary btn-xs">History Botol</a>
											<?php endif; ?>
										</td>
                                    </tr>
									<?php $ourut++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                    <div class="box-footer clearfix">
                        &nbsp;
                    </div>
                </div>

                <!-- Form order /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Order Form</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
                        <form role="form" action="" method="post">

							<div class="row">
								<div class="col-md-12">
									<h4 class="box-title">New Order</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">

                                    <div class="form-group">
										<label class="control-label">Partner</label>
										<select name="p_id" class="form-control select2" reguired>
											<option value=""></option>
											<?php foreach ($partner as $partner_list): ?>
                                            <?php $data['el_membership']    = $this->admin_master_model->get_row('el_membership','em_id',$partner_list['em_id']); ?>
												<option value="<?=$partner_list['p_id']?>" <?=$s_partner?>><?=$partner_list['p_name']?> - <?=$data['el_membership']['em_code']?> - <?=$partner_list['p_phone']?></option>
											<?php endforeach; ?>
										</select>
									</div>
								
								</div>
								<div class="col-md-6">
								
                                    <div class="form-group">
										<label class="control-label">Paket Bisnis</label>
										<select name="ebp_id" class="form-control select2" reguired>
											<option value=""></option>
											<?php foreach ($el_bussiness_package as $el_bussiness_package_list): ?>
                                            <?php $data['el_product'] = $this->admin_master_model->get_row('el_product','ep_id',$el_bussiness_package_list['ep_id']); ?>
												<option value="<?=$el_bussiness_package_list['ebp_id']?>"><?=$data['el_product']['ep_name']?> - <?=$el_bussiness_package_list['ebp_code']?>: RP <?=number_format($el_bussiness_package_list['ebp_price'],0,",",".")?></option>
											<?php endforeach; ?>
										</select>
									</div>
								
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" name="order_btn" value="order" class="btn btn-info">Order</button>
								</div>
							</div>

                        </form>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->