		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?=$cont_head_h1?></h1>
				<ol class="breadcrumb">
					<?=$breadcrumb?>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <?php
                    if (isset($_POST['partner_btn_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";
                    }
                ?>
                <?php if (isset($notif_partner)): ?>
                    <div class="row">
						<div class="col-md-12">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-info"></i> Notification</h4>
								<?=$notif_partner?>
							</div>
						</div>
					</div>
                <?php endif; ?>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Diri</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">

                        <table class="table">
                            <tr>
                                <td width=35%>Status Partnership</td>
                                <td><?=$user_perm['name']?></td>
                            </tr>
                            <tr>
                                <td>Tanggal & Waktu Bergabung</td>
                                <td><?=date('d F Y', strtotime($user_info['p_joindate']))?>, pukul <?=date('H:i:s', strtotime($user_info['p_joindate']))?></td>
                            </tr>
                            <tr>
                                <td>Nama Lengkap</td>
                                <td><?=$user_info['p_name']?></td>
                            </tr>
                            <tr>
                                <td>Alamat Lengkap</td>
                                <td><?=$user_info['p_address']?></td>
                            </tr>
                            <tr>
                                <td>Kota</td>
                                <td><?=$regencies['name']?></td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td><?=$provinces['name']?></td>
                            </tr>
                            <tr>
                                <td>Nomor Telephone</td>
                                <td><?=$user_info['p_phone']?></td>
                            </tr>
                            <tr>
                                <td>Alamat Email</td>
                                <td><?=$user_info['username']?></td>
                            </tr>
                            <tr>
                                <td>Keanggotaan</td>
                                <td><?=$membership['em_name']?></td>
                            </tr>
                            <tr>
                                <td>Kode ID</td>
                                <td><?=$membership['em_code']?></td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Transaksi</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">

                        <table id="example1" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu dan tanggal pengambilan</th>
                                    <th>Ukuran Botol</th>
                                    <th>Jumlah pengambilan</th>
                                    <th>Jumlah botol elcure yang sudah diterima</th>
                                    <th>Harga per Botol</th>
                                    <th>Total Pembelian</th>
                                    <th>Yang sudah dibayarkan ke perusahaan</th>
                                    <th>Status Pembayaran</th>
                                    <th>Keterangan</th>
                                    <th>Upload Bukti Transfer</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $o_urut = 1; ?>
                                <?php foreach ($partner_order as $partner_order_list): ?>
                                    <?php $data['partner']              = $this->admin_master_model->get_row('partner','p_id',$partner_order_list['p_id']); ?>
                                    <?php $data['el_bussiness_package'] = $this->admin_master_model->get_row('el_bussiness_package','ebp_id',$partner_order_list['ebp_id']); ?>
                                    <?php $data['el_membership']        = $this->admin_master_model->get_row('el_membership','em_id',$data['el_bussiness_package']['em_id']); ?>
                                    <?php $ec_field = explode(',', $data['el_bussiness_package']['ec_field']); ?>
                                    <tr>
                                        <td><?=$o_urut?></td>
                                        <td><?=date('d F Y', strtotime($partner_order_list['po_createdate']))?>, pukul <?=date('H:i:s', strtotime($partner_order_list['po_createdate']))?></td>
                                        <td><?=$partner_order_list['ep_name']?></td>
                                        <td>
                                            <?php
                                                foreach ($ec_field as $ec_field_botol)
                                                {
                                                    $ec_field_botol_par = explode(':', $ec_field_botol);
                                                    $data['el_customfield'] = $this->admin_master_model->get_row('el_customfield','ec_id',$ec_field_botol_par[0]);
                                                    if ($data['el_customfield']['ec_declar_cat'] == 'botol')
                                                    {
                                                        echo $data['el_customfield']['ec_field'].": ".number_format($ec_field_botol_par[1],0,",",".")."<hr>";
                                                    }
                                                }
                                            ?>                                
                                        </td>
                                        <td></td>
                                        <td>
                                            <?php
                                                foreach ($ec_field as $ec_field_harga)
                                                {
                                                    $ec_field_harga_par = explode(':', $ec_field_harga);
                                                    $data['el_customfield'] = $this->admin_master_model->get_row('el_customfield','ec_id',$ec_field_harga_par[0]);
                                                    if ($data['el_customfield']['ec_declar_cat'] == 'harga')
                                                    {
                                                        echo $data['el_customfield']['ec_field'].": ".number_format($ec_field_harga_par[1],0,",",".")."<hr>";
                                                    }
                                                }
                                            ?>                                
                                        </td>
                                        <td><?=number_format($partner_order_list['ebp_price'],0,",",".")?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php $o_urut++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->