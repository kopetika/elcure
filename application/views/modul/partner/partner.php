		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?=$cont_head_h1?></h1>
				<ol class="breadcrumb">
					<?=$breadcrumb?>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <?php
                    if (isset($_POST['partner_btn_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";
                    }
                ?>
                <?php if (isset($notif_partner)): ?>
                    <div class="row">
						<div class="col-md-12">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-info"></i> Notification</h4>
								<?=$notif_partner?>
							</div>
						</div>
					</div>
                <?php endif; ?>

				<?php if ($this->session->is_admin == 1): ?>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Partner</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body table-responsive no-padding">

							<table id="example1" class="table">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Keagenan</th>
										<th>Kode ID</th>
										<th>Telephone</th>
										<th>Alamat</th>
										<th>Kota</th>
										<th>Provinsi</th>
										<th>Tanggal Join</th>
										<th>Upline</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($partner as $partner_list): ?>
									<?php $data['el_membership']    = $this->admin_master_model->get_row('el_membership','em_id',$partner_list['em_id']); ?>
									<?php $data['locat_provinces']  = $this->admin_master_model->get_row('locat_provinces','id',$partner_list['lp_id']); ?>
									<?php $data['locat_regencies']  = $this->admin_master_model->get_row('locat_regencies','id',$partner_list['lr_id']); ?>
									<?php $data['parent_detail']	= $this->admin_master_model->get_row('partner','p_id',$partner_list['p_id_parent']); ?>
									<?php
										if ($partner_list['p_id_parent'] == 0)
										{
											$partner_name	= 'Perusahaan';
											$partner_email	= '';
										}
										else
										{
											$partner_name	= $data['parent_detail']['p_name'];
											$partner_email	= $data['parent_detail']['username'];
										}
									?>
										<tr>
											<td><?=$partner_list['p_name']?></td>
											<td><?=$data['el_membership']['em_name']?></td>
											<td><?=$data['el_membership']['em_code']?></td>
											<td><?=$partner_list['p_phone']?></td>
											<td><?=$partner_list['p_address']?></td>
											<td><?=$data['locat_regencies']['name']?></td>
											<td><?=$data['locat_provinces']['name']?></td>
											<td><?=$partner_list['p_joindate']?></td>
											<td><?=$partner_name?> (<?=$partner_email?>)</td>
											<td>
												<a href="<?=site_url()?>/bussiness/summary?usr=<?=$partner_list['p_id']?>" class="btn btn-danger btn-xs" target="_blank">Show</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

						</div>
						<div class="box-footer clearfix">
							&nbsp;
						</div>
					</div>
				<?php endif; ?>

				<?php if (isset($_GET['check_postalcode'])): ?>
					<!-- Form registration /Default box -->
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Registration Form</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<form role="form" action="" method="post">

								<div class="row">
									<div class="col-md-12">
										<h4 class="box-title">Upline</h4>
										<table class="table" id="example5">
											<thead>
												<tr>
													<th></th>
													<th>Email</th>
													<th>Nama</th>
													<th>Jumlah Downline</th>
												</tr>
											</thead>
											<tbody>
												<?php if ($check_parent): ?>
													<?php foreach ($check_parent as $check_parent_list): ?>
														<?php $data['jml_downline']= $this->admin_master_model->get_table_num('partner','p_id_parent',$check_parent_list['p_id']); ?>
														<?php $jml_downline[] = $data['jml_downline']; ?>
														<?php $jml_downline_min = min($jml_downline); ?>
														<?php if ($data['jml_downline'] == $jml_downline_min): ?>
															<tr>
																<td><input type="radio" name="p_id" value="<?=$check_parent_list['p_id']?>" required></td>
																<td><?=$check_parent_list['username']?></td>
																<td><?=$check_parent_list['p_name']?></td>
																<td><?=$data['jml_downline']?></td>
															</tr>
														<?php  endif; ?>
													<?php endforeach; ?>
												<?php else: ?>
													<tr>
														<td><input type="radio" name="p_id" value="0" required></td>
														<td>Perusahaan</td>
														<td></td>
														<td></td>
													</tr>
												<?php endif; ?>
											</tbody>
										</table>
										<?=$jml_downline_min?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h4 class="box-title">Data Pribadi</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
									
										<div class="form-group">
											<label class="control-label">Keagenan</label>
											<select name="em_id" class="form-control select2" reguired>
												<option value=""></option>
												<?php foreach ($el_membership as $el_membership_list): ?>
													<option value="<?=$el_membership_list['em_id']?>"><?=$el_membership_list['em_name']?> - <?=$el_membership_list['em_code']?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Nama</label>
											<input type="text" name="p_name" class="form-control" placeholder="Nama" required>
										</div>
										<div class="form-group">
											<label class="control-label">Email</label>
											<input type="email" name="username" class="form-control" placeholder="Username - Email" required>
										</div>
										<div class="form-group">
											<label class="control-label">Telephone</label>
											<input type="text" name="p_phone" class="form-control" placeholder="Telephone" required>
										</div>
										<div class="form-group">
											<label class="control-label">Password</label>
											<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
											<p class="help-block"><input type="checkbox" onclick="showPass()"> Show Password</p>
										</div>
									
									</div>
									<div class="col-md-6">
									
										<div class="form-group">
											<label class="control-label">Alamat</label>
											<textarea name="p_address" class="form-control"></textarea>
										</div>
										<div class="form-group">
											<label class="control-label">Provinsi</label>
											<select id="lp_id" name="lp_id" class="form-control select2" reguired>
												<option value=""></option>
												<?php foreach ($locat_provinces as $locat_provinces_list): ?>
													<option value="<?=$locat_provinces_list['id']?>"><?=$locat_provinces_list['name']?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Kota</label>
											<select id="lr_id" name="lr_id" class="form-control select2" reguired>
												<option value=""></option>
												<?php foreach ($locat_regencies as $locat_regencies_list): ?>
													<option value="<?=$locat_regencies_list['id']?>"><?=$locat_regencies_list['name']?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Kode Pos</label>
											<input type="text" name="p_postalcode" class="form-control" placeholder="Kode Pos" value="<?=$_GET['p_postalcode']?>" required>
										</div>
									
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<a href="<?=current_url()?>" class="btn btn-warning">Cancel</a>
										<button type="submit" name="partner_btn" value="partner" class="btn btn-info">Submit</button>
									</div>
								</div>

							</form>
						</div><!-- /.box-body -->
						<div class="box-footer clearfix">
							&nbsp;
						</div><!-- /.box-footer-->
					</div><!-- /.box -->
				<?php else: ?>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Registration Form</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<form role="form" action="" method="get">

								<div class="row">
									<div class="col-md-12">
										<h4 class="box-title">New Partner</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
									
										<div class="form-group">
											<label class="control-label">Kode Pos</label>
											<input type="text" name="p_postalcode" class="form-control" placeholder="Kode Pos" required>
										</div>
									
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" name="check_postalcode" value="check_postalcode" class="btn btn-info">Next</button>
									</div>
								</div>

							</form>
						</div><!-- /.box-body -->
						<div class="box-footer clearfix">
							&nbsp;
						</div><!-- /.box-footer-->
					</div><!-- /.box -->
				<?php endif; ?>

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->

<!-- jQuery 2.1.4 -->
<script src="<?=base_url()?>application/views/themeAdmin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script type="text/javascript">

    $(function(){

        $.ajaxSetup({
            type:"POST",
            url: "<?=site_url()?>/bussiness/select_location",
            cache: false,
        });

        //Product
        $("#lp_id").change(function(){
            var value=$(this).val();
            if(value>0){
                $.ajax({
                    data:{modul:'kabupaten',id:value},
                    success: function(respond){
                        $("#lr_id").html(respond);
                    }
                })
            }
        });

    });

    function showPass() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}

</script>