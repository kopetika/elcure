		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?=$cont_head_h1?></h1>
				<ol class="breadcrumb">
					<?=$breadcrumb?>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <?php
                    if (isset($_POST['payment_history_btn_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";

						foreach ($_POST as $key => $value) {
							echo $key[0]."<br />";
						}
                    }

					
                ?>
                <?php if (isset($notif_order)): ?>
                    <div class="row">
						<div class="col-md-12">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-info"></i> Notification</h4>
								<?=$notif_order?>
							</div>
						</div>
					</div>
                <?php endif; ?>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Partner Order</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table id="example1" class="table">
                            <thead>
                                <tr>
									<th></th>
                                    <th>Nama</th>
                                    <th>Telephone</th>
                                    <th>Paket Bisnis</th>
                                    <th>Keagenan</th>
                                    <th>Kode ID</th>
                                    <th>Harga</th>
                                    <th>Tanggal Order</th>
									<th>Alamat</th>
									<th>Kota</th>
									<th>Provinsi</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php $ourut = 1; ?>
                                <?php foreach ($partner_order as $partner_order_list): ?>
                                <?php $data['partner']              = $this->admin_master_model->get_row('partner','p_id',$partner_order_list['p_id']); ?>
                                <?php $data['el_bussiness_package'] = $this->admin_master_model->get_row('el_bussiness_package','ebp_id',$partner_order_list['ebp_id']); ?>
                                <?php $data['el_membership']        = $this->admin_master_model->get_row('el_membership','em_id',$data['el_bussiness_package']['em_id']); ?>
                                <?php $data['locat_provinces']  	= $this->admin_master_model->get_row('locat_provinces','id',$data['partner']['lp_id']); ?>
                                <?php $data['locat_regencies']  	= $this->admin_master_model->get_row('locat_regencies','id',$data['partner']['lr_id']); ?>
                                    <tr>
										<td><?=$ourut?></td>
                                        <td><?=$data['partner']['p_name']?></td>
                                        <td><?=$data['partner']['p_phone']?></td>
                                        <td><?=$partner_order_list['ep_name']?></td>
                                        <td><?=$data['el_membership']['em_name']?></td>
                                        <td><?=$partner_order_list['ebp_code']?></td>
                                        <td><?=number_format($partner_order_list['ebp_price'],0,",",".")?></td>
                                        <td><?=date('d F Y', strtotime($partner_order_list['po_createdate']))?>, pukul <?=date('H:i:s', strtotime($partner_order_list['po_createdate']))?></td>
										<td><?=$data['partner']['p_address']?></td>
										<td><?=$data['locat_regencies']['name']?></td>
										<td><?=$data['locat_provinces']['name']?></td>
                                    </tr>
									<?php $ourut++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                    <div class="box-footer clearfix">
                        &nbsp;
                    </div>
                </div>

                <!-- Form order /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">History Pembayaran</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
                        <form role="form" action="" method="post">

							<div class="row">
								<div class="col-md-6">
									<?php foreach ($partner_order_calc as $partner_order_calc_list): ?>
										<?php $ec_declar_arr = explode('_', $partner_order_calc_list['ec_declar']) ?>

										<h4 class="box-title">Pembayaran Botol <?=$ec_declar_arr[1]?> ml</h4>
									
										<input type="text" name="0['ec_id']" value="<?=$partner_order_calc_list['ec_id']?>">
										<input type="text" name="1['ec_declar']" value="<?=$partner_order_calc_list['ec_declar']?>">
										<input type="text" name="2['ec_declar_cat']" value="<?=$partner_order_calc_list['ec_declar_cat']?>">

										<div class="form-group">
											<label class="control-label">Pembayaran</label>
											<input type="number" name="3['pohp_payment']" class="form-control">
										</div>
										<div class="form-group">
											<label class="control-label">Status Pembayaran</label>
											<input type="text" name="4['pohp_status']" class="form-control">
										</div>
										<div class="form-group">
											<label class="control-label">Keterangan</label>
											<input type="text" name="5['pohp_description']" class="form-control">
										</div>
										<div class="form-group">
											<label class="control-label">Tanggal Pembayaran</label>
											<input type="date" name="6['pohp_datetime']" class="form-control">
										</div>
										<hr>
										
									<?php endforeach; ?>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<button type="submit" name="payment_history_btn_debug" value="payment_history" class="btn btn-info">Order</button>
								</div>
							</div>

                        </form>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->