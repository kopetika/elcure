		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?=$cont_head_h1?></h1>
				<ol class="breadcrumb">
					<?=$breadcrumb?>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <?php
                    if (isset($_POST['btn_package_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";
                        foreach ($_POST['ec_field'] as $key => $value)
                        {
                            $ec_field[] = $key.":".$value;
                        }
                        echo "<pre>";
                        print_r($ec_field);
                        echo "</pre>";
                        echo implode(',',$ec_field);
                    }

                    if (isset($_POST['btn_package_edit_debug']))
                    {
                        echo "<pre>";
                        print_r($_POST);
                        echo "</pre>";
                        foreach ($_POST['ec_field'] as $key => $value)
                        {
                            $ec_field[] = $key.":".$value;
                        }
                        echo "<pre>";
                        print_r($ec_field);
                        echo "</pre>";
                        echo implode(',',$ec_field);
                    }
                ?>
                <?php if (isset($notif_package)): ?>
                    <div class="row">
						<div class="col-md-12">
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-info"></i> Notification</h4>
								<?=$notif_package?>
							</div>
						</div>
					</div>
                <?php endif; ?>

                <?php foreach ($el_product as $el_product_list): ?>
                    <?php $data['el_customfield']       = $this->admin_master_model->get_result_where('el_customfield','ep_id',$el_product_list['ep_id']); ?>
                    <?php $data['el_bussiness_package'] = $this->admin_master_model->get_result_where('el_bussiness_package','ep_id',$el_product_list['ep_id']); ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?=$el_product_list['ep_name']?></h3>
                            <div class="box-tools pull-right">
                                <a href="#" data-toggle="modal" data-target="#addPackage<?=$el_product_list['ep_id']?>" class="btn btn-primary btn-xs"><span data-toggle="tooltip" title="Tambah paket bisnis"><i class="fa fa-plus"></i> Paket bisnis</span></a>
                                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Keagenan</th>
                                    <th style="text-align: center;">Kode ID</th>
                                    <?php foreach ($data['el_customfield'] as $el_customfield): ?>
                                        <th style="text-align: center;"><?=$el_customfield['ec_field']?></th>
                                    <?php endforeach ?>
                                    <th style="text-align: right;">Harga</th>
                                    <th></th>
                                </tr>
                                <?php foreach ($data['el_bussiness_package'] as $el_bussiness_package): ?>
                                <?php $data['el_membership']    = $this->admin_master_model->get_row('el_membership','em_id',$el_bussiness_package['em_id']); ?>
                                <?php $ec_field_array           = explode(',',$el_bussiness_package['ec_field']); ?>
                                    <tr>
                                        <td><?=$data['el_membership']['em_name']?></td>
                                        <td style="text-align: center;"><?=$el_bussiness_package['ebp_code']?></td>
                                        <?php foreach ($ec_field_array as $ec_key => $ec_value): ?>
                                        <?php $ec_value_array = explode(':',$ec_value); ?>
                                            <td style="text-align: center;"><?=number_format($ec_value_array[1],0,",",".")?></td>
                                        <?php endforeach; ?>
                                        <td style="text-align: right;"><?=number_format($el_bussiness_package['ebp_price'],0,",",".")?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editPackage<?=$el_bussiness_package['ebp_id']?>" class="btn btn-warning btn-xs">Edit</a>
                                            <!-- Modal edit paket bisnis -->
                                            <div class="modal fade" id="editPackage<?=$el_bussiness_package['ebp_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form class="form-horizontal" action="" method="post">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Tambah paket bisnis</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                
                                                                <input type="hidden" name="ebp_id" value="<?=$el_bussiness_package['ebp_id']?>">
                                                                <input type="hidden" name="ep_id" value="<?=$el_bussiness_package['ep_id']?>">
                                                                <div class="form-group" style="padding: 0 15px;">
                                                                    <label class="control-label" for="nama">Keagenan</label>
                                                                    <select name="em_id" class="form-control" required>
                                                                        <option value=""></option>
                                                                        <?php foreach ($el_membership as $el_membership_list): ?>
                                                                        <?php
                                                                            $s_membership = '';
                                                                            if ($el_membership_list['em_id'] == $el_bussiness_package['em_id'])
                                                                            {
                                                                                $s_membership = 'selected';
                                                                            }
                                                                        ?>
                                                                            <option value="<?=$el_membership_list['em_id']?>" <?=$s_membership?>><?=$el_membership_list['em_name']?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group" style="padding: 0 15px;">
                                                                    <label class="control-label">Kode ID</label>
                                                                    <input type="text" name="ebp_code" class="form-control" value="<?=$el_bussiness_package['ebp_code']?>" required>
                                                                </div>
                                                                <?php foreach ($ec_field_array as $ec_key_edit => $ec_value_edit): ?>
                                                                <?php $ec_value_edit_array = explode(':',$ec_value_edit); ?>
                                                                <?php $data['el_customfield_edit']    = $this->admin_master_model->get_row('el_customfield','ec_id',$ec_value_edit_array[0]); ?>
                                                                    <div class="form-group" style="padding: 0 15px;">
                                                                        <label class="control-label"><?=$data['el_customfield_edit']['ec_field']?></label>
                                                                        <input type="text" name="ec_field[<?=$ec_value_edit_array[0]?>]" value="<?=$ec_value_edit_array[1]?>" class="form-control" required>
                                                                    </div>
                                                                <?php endforeach ?>
                                                                <div class="form-group" style="padding: 0 15px;">
                                                                    <label class="control-label">Harga</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">Rp</span>
                                                                        <input type="number" name="ebp_price" class="form-control" value="<?=$el_bussiness_package['ebp_price']?>" required>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" name="btn_package_edit" value="package_edit" data-toggle="tooltip" title="Edit paket bisnis" class="btn btn-danger">
                                                                    Submit
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                    </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                            &nbsp;
                        </div>
                    </div>
                    <!-- Modal tambah paket bisnis -->
                    <div class="modal fade" id="addPackage<?=$el_product_list['ep_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="form-horizontal" action="" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Tambah paket bisnis</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        <input type="hidden" name="ep_id" value="<?=$el_product_list['ep_id']?>">
                                        <div class="form-group" style="padding: 0 15px;">
                                            <label class="control-label" for="nama">Keagenan</label>
                                            <select name="em_id" class="form-control" required>
                                                <option value=""></option>
                                                <?php foreach ($el_membership as $el_membership_list): ?>
                                                    <option value="<?=$el_membership_list['em_id']?>"><?=$el_membership_list['em_name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group" style="padding: 0 15px;">
                                            <label class="control-label">Kode ID</label>
                                            <input type="text" name="ebp_code" class="form-control" required>
                                        </div>
                                        <?php foreach ($data['el_customfield'] as $el_customfield_input): ?>
                                            <div class="form-group" style="padding: 0 15px;">
                                                <label class="control-label"><?=$el_customfield_input['ec_field']?></label>
                                                <input type="text" name="ec_field[<?=$el_customfield_input['ec_id']?>]" class="form-control" required>
                                            </div>
                                        <?php endforeach ?>
                                        <div class="form-group" style="padding: 0 15px;">
                                            <label class="control-label">Harga</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="number" name="ebp_price" class="form-control" required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" name="btn_package" value="package" data-toggle="tooltip" title="Tambah paket bisnis" class="btn btn-danger">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
