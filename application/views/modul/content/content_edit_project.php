<?php
	echo form_open('content_admin/content/'.$this->uri->segment(3).'/'.$this->uri->segment(4));

	echo form_hidden('id', $this->uri->segment(5));

	echo '<div class="form-group">';
	echo form_label('Judul', 'menu');
	echo form_input('menu', $content_edit['menu'], 'class="form-control" onChange="writeMenu(this.form)" autofocus');
	echo '</div/>';

	echo form_hidden('title', $content_edit['title']);

	echo form_hidden('slug', $content_edit['slug']);

	//echo '<div class="form-group">';
	//echo form_label('Content', 'content');
	//echo form_textarea('content', $content_edit['content'], 'id="editor1" ');
	//echo '</div>';
?>
	<div class="form-group">
		<label for="kategori">Kategori</label>
		<select class="form-control" name="sub_content">
			<option value=""></option>
			<?php foreach ($content_cat as $content_cat_list): ?>
				<?php
					$contcat_s = '';
					if ($content_cat_list['id'] == $content_edit['sub_content'])
					{
						$contcat_s = 'selected';
					}
				?>
				<?php $data['content_cat_sub']	= $this->content_admin_model->content_kategori($content_cat_list['id'],$this->uri->segment(3)); ?>
				<option value="<?=$content_cat_list['id']?>" <?=$contcat_s?>><?=$content_cat_list['name']?></option>
				<?php foreach ($data['content_cat_sub'] as $content_cat_sub_list): ?>
					<?php
						$contcat_s_sub = '';
						if ($content_cat_sub_list['id'] == $content_edit['sub_content'])
						{
							$contcat_s_sub = 'selected';
						}
					?>
					<option value="<?=$content_cat_sub_list['id']?>" <?=$contcat_s_sub?>>- <?=$content_cat_sub_list['name']?></option>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</select>
	</div>

	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab">Lokasi Penempatan Kamera</a></li>
					<li><a href="#tab_2" data-toggle="tab">Sistem & Topologi</a></li>
					<li><a href="#tab_3" data-toggle="tab">Feature Teknologi</a></li>
					<li><a href="#tab_4" data-toggle="tab">Produk</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<textarea name="content" id="editor1"><?=$content_edit['content']?></textarea>
					</div>
					<div class="tab-pane" id="tab_2">
						<textarea name="content_sis" id="editor2"><?=$content_edit['content_sys']?></textarea>
					</div>
					<div class="tab-pane" id="tab_3">
						<textarea name="content_tech" id="editor3"><?=$content_edit['content_tech']?></textarea>
					</div>
					<div class="tab-pane" id="tab_4">
						<textarea name="content_prod" id="editor4"><?=$content_edit['content_prod']?></textarea>
					</div>
				</div>
			</div>

		</div>
	</div>
<?php

	echo form_hidden('img', $content_edit['img']);

	echo form_hidden('content_type', $this->uri->segment(3));

	echo form_hidden('content_pos', $this->uri->segment(4));

	echo form_hidden('content_by', $this->session->userdata('username'));

	echo '<div class="form-group">';
	echo form_label('&nbsp;', '&nbsp;');
	echo form_submit('btn_content_project_edit', 'Submit', 'class="btn btn-info"');
	echo '</div>';

	echo form_close();
?>
<SCRIPT LANGUAGE="JavaScript">
function writeMenu (form) {
	var menu = form.menu.value;
    form.title.value = menu;
}
</SCRIPT>
