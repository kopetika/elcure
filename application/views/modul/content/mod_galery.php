<?php
    if (isset($cat_slug))
    {
        $cat_title = $cat_slug['name'];
        $cat_desc  = $cat_slug['cat_desc'];
    }
    else
    {
        $cat_title = $page_title;
        $cat_desc  = '';
    }

?>
<style>
    .img-thumb{
        width: 356px;
        height: 200px;
        object-fit: cover;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h2 class="mt-2" style="text-transform:capitalize; font-weight:100;"><?=$cat_title?></h2>
            <p><?=$cat_desc?></p>
        </div>
    </div>
</div>

<div class="container">
    <!-- Gallery -->
    <div class="row">
        <?php foreach ($galery as $galery_list): ?>
            <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                <a href="#" data-bs-toggle="modal" data-bs-target="#galleryModal<?=$galery_list['id']?>">
                    <img
                    src="<?=base_url()?>user_upload/<?=$galery_list['img']?>"
                    class="w-100 shadow-1-strong rounded mb-4 img-thumb hover-shadow"
                    alt=""
                    />
                </a>
                <div class="modal fade" id="galleryModal<?=$galery_list['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen">
                        <div class="modal-content" style="background-color: #000000c9;">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel" style="color:#a9a9a9;"><?=$galery_list['title']?></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <center>
                                    <img
                                    src="<?=base_url()?>user_upload/<?=$galery_list['img']?>"
                                    class="shadow-1-strong rounded mb-4"
                                    alt=""
                                    />
                                </center>
                            </div>
                            <div class="modal-footer border-0">
                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        

    </div>
    <!-- Gallery -->
</div>