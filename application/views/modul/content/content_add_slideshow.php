<?php if (isset($notif_content_add)): ?>
	<div class='callout callout-danger'>
		<?=$notif_content_add?><br />
		<a href="<?=site_url()?>/content_admin/content/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>" class="btn btn-warning btn-xs">Back</a>
	</div>
<?php endif; ?>

<form action="" method="post">
<?php
	echo form_hidden('id');

	echo form_hidden('sub_content');

	echo '<div class="form-group">';
	echo form_label('Judul', 'menu');
	echo form_input('menu', '', 'class="form-control" onChange="writeMenu(this.form)" autofocus');
	echo '</div/>';

	echo form_hidden('title');

	echo form_hidden('slug');

	echo '<div class="form-group">';
	echo form_label('Content', 'content');
	echo form_textarea('content', '', 'id="editor1" ');
	echo '</div>';

	//echo form_hidden('img');

	echo form_hidden('content_type', $this->uri->segment(3));

	echo form_hidden('content_pos', $this->uri->segment(4));

	echo form_hidden('content_by', $this->session->userdata('email'));

	echo '<div class="form-group">';
	echo form_label('&nbsp;', '&nbsp;');
	echo form_submit('btn_content', 'Submit', 'class="btn btn-info"');
	echo '</div>';
?>
</form>
<SCRIPT LANGUAGE="JavaScript">
function writeMenu (form) {
	var menu = form.menu.value;
    form.title.value = menu;
}
</SCRIPT>
