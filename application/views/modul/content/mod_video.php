<?php
    if (isset($cat_slug))
    {
        $cat_title = $cat_slug['name'];
        $cat_desc  = $cat_slug['cat_desc'];
    }
    else
    {
        $cat_title = 'Video';
        $cat_desc  = '';
    }

?>
<style>
    .videowrapper { float: none; clear: both; width: 100%; position: relative; padding-bottom: 56.25%; padding-top: 25px; height: 0; }
    .videowrapper iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h2 class="mt-2" style="text-transform:capitalize; font-weight:100;"><?=$cat_title?></h2>
            <p><?=$cat_desc?></p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <?php foreach ($video as $video_list): ?>
            <div class="col-lg-6 col-md-12 mb-4 mb-lg-10 mb-10">
                <div class="videowrapper"><?=$video_list['content']?></div>
            </div>
        <?php endforeach; ?>

    </div>
</div>