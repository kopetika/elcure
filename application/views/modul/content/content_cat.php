		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Content
					<small>Content Manager</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="<?=site_url()?>/content_admin/content/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>"><?=$this->uri->segment(3)?></a></li>
					<li class="active">category</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<?php include('content_manage_menu.php'); ?>

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Kategori Kontent</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">

						<table class="table table-hover">
							<tr>
								<th>Kategori</th>
								<th></th>
								<th></th>
							</tr>
							<?php foreach ($content_cat as $content_cat_list): ?>
								<tr>
									<td><?=$content_cat_list['name']?></td>
									<td>

										<a href="#" data-toggle="modal" data-target="#catAdd_<?=$content_cat_list['id']?>"><span class="glyphicon glyphicon-plus text-green" data-toggle="tooltip" title="Add Sub Category for <?=$content_cat_list['name']?>"></span></a>
										<!-- Modal -->
										<div class="modal fade" id="catAdd_<?=$content_cat_list['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="myModalLabel">Add Kategori</h4>
													</div>
													<form action="" method="post">
													<div class="modal-body">
														<input type="hidden" name="par_id" value="<?=$content_cat_list['id']?>">
														<div class="form-group">
															<input type="text" name="cat_name" class="form-control" value="" placeholder="Input kategori">
														</div>
														<div class="form-group">
															<textarea name="cat_desc" class="form-control" rows="8" placeholder="Input deskripsi singkat"></textarea>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
														<button type="submit" name="btn_catadd" value="catadd" class="btn btn-danger">
															Add
														</button>
													</div>
													</form>
												</div>
											</div>
										</div>

									</td>
									<td>

										<a href="<?=site_url()?>/content_admin/category_foto/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content_cat_list['id']?>" data-toggle="tooltip" title="Edit Image" disabled><span class="glyphicon glyphicon-picture text-green"></a>
										<a href="#" data-toggle="modal" data-target="#catEdit_<?=$content_cat_list['id']?>"><span class="glyphicon glyphicon-edit text-blue" data-toggle="tooltip" title="Edit Kategori"></span></a>
										<!-- Modal -->
										<div class="modal fade" id="catEdit_<?=$content_cat_list['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
													</div>
													<form action="" method="post">
													<div class="modal-body">
														<input type="hidden" name="id" value="<?=$content_cat_list['id']?>">
														<div class="form-group">
															<input type="text" name="cat_name" class="form-control" value="<?=$content_cat_list['name']?>">
														</div>
														<div class="form-group">
															<textarea name="cat_desc" class="form-control" rows="8" placeholder="Input deskripsi singkat"><?=$content_cat_list['cat_desc']?></textarea>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
														<button type="submit" name="btn_catedit" value="catedit" class="btn btn-danger">
															Edit
														</button>
													</div>
													</form>
												</div>
											</div>
										</div>

										<!--
										<?php $cek_child = $this->content_admin_model->content_kategori_cek_child($content_cat_list['id']); ?>
										<?php if ($cek_child == 0): ?>
											<a href="#" data-toggle="modal" data-target="#catDel_<?=$content_cat_list['id']?>"><span class="glyphicon glyphicon-trash text-red" data-toggle="tooltip" title="Edit Kategori"></span></a>
											<div class="modal fade" id="catDel_<?=$content_cat_list['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title" id="myModalLabel">Delete Kategori</h4>
														</div>
														<form action="" method="post">
														<div class="modal-body">
															<p>Anda yakin akan menghapus kategori berikut?</p>
															<input type="hidden" name="id" value="<?=$content_cat_list['id']?>">
															<div class="form-group">
																<input type="text" name="cat_name" class="form-control" value="<?=$content_cat_list['name']?>" disabled>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
															<button type="submit" name="btn_catdel" value="catdel" class="btn btn-danger">
																Delete
															</button>
														</div>
														</form>
													</div>
												</div>
											</div>
										<?php endif; ?>
										-->

									</td>
								</tr>
								<?php $data['content_cat_l2'] = $this->content_admin_model->content_kategori($content_cat_list['id'],$this->uri->segment(3)); ?>
								<?php foreach ($data['content_cat_l2'] as $content_cat_l2): ?>
									<tr>
										<td style="padding-left: 25px;"><?=$content_cat_l2['name']?></td>
										<td>

											<!--
											<a href="#" data-toggle="modal" data-target="#catAdd_<?=$content_cat_l2['id']?>"><span class="glyphicon glyphicon-plus text-green" data-toggle="tooltip" title="Add Sub Category for <?=$content_cat_l2['name']?>"></span></a>
											<div class="modal fade" id="catAdd_<?=$content_cat_l2['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title" id="myModalLabel">Add Kategori</h4>
														</div>
														<form action="" method="post">
														<div class="modal-body">
															<input type="hidden" name="par_id" value="<?=$content_cat_l2['id']?>">
															<div class="form-group">
																<input type="text" name="cat_name" class="form-control" value="">
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
															<button type="submit" name="btn_catadd" value="catadd" class="btn btn-danger">
																Add
															</button>
														</div>
														</form>
													</div>
												</div>
											</div>
											-->

										</td>
										<td>

											<a href="<?=site_url()?>/content_admin/category_foto/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content_cat_l2['id']?>" data-toggle="tooltip" title="Edit Image"><span class="glyphicon glyphicon-picture text-green"></a>
											<a href="#" data-toggle="modal" data-target="#catEdit_<?=$content_cat_l2['id']?>"><span class="glyphicon glyphicon-edit text-blue" data-toggle="tooltip" title="Edit Kategori"></span></a>
											<!-- Modal -->
											<div class="modal fade" id="catEdit_<?=$content_cat_l2['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
														</div>
														<form action="" method="post">
														<div class="modal-body">
															<input type="hidden" name="id" value="<?=$content_cat_l2['id']?>">
															<div class="form-group">
																<input type="text" name="cat_name" class="form-control" value="<?=$content_cat_l2['name']?>">
															</div>
															<div class="form-group">
																<textarea name="cat_desc" class="form-control" rows="8" placeholder="Input deskripsi singkat"><?=$content_cat_l2['cat_desc']?></textarea>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
															<button type="submit" name="btn_catedit" value="catedit" class="btn btn-danger">
																Edit
															</button>
														</div>
														</form>
													</div>
												</div>
											</div>

											<?php $cek_child_l2 = $this->content_admin_model->content_kategori_cek_child($content_cat_l2['id']); ?>
											<?php if ($cek_child_l2 == 0): ?>
												<a href="#" data-toggle="modal" data-target="#catDel_<?=$content_cat_l2['id']?>"><span class="glyphicon glyphicon-trash text-red" data-toggle="tooltip" title="Edit Kategori"></span></a>
												<!-- Modal -->
												<div class="modal fade" id="catDel_<?=$content_cat_l2['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																<h4 class="modal-title" id="myModalLabel">Delete Kategori</h4>
															</div>
															<form action="" method="post">
															<div class="modal-body">
																<p>Anda yakin akan menghapus kategori berikut?</p>
																<input type="hidden" name="id" value="<?=$content_cat_l2['id']?>">
																<div class="form-group">
																	<input type="text" name="cat_name" class="form-control" value="<?=$content_cat_l2['name']?>" disabled>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
																<button type="submit" name="btn_catdel" value="catdel" class="btn btn-danger">
																	Delete
																</button>
															</div>
															</form>
														</div>
													</div>
												</div>
											<?php endif; ?>

										</td>
									</tr>
								<?php endforeach; ?>
							<?php endforeach; ?>
							<!--
							<tr>
								<td>

									<a href="#" data-toggle="modal" data-target="#catAdd_par" class="btn btn-default"><span data-toggle="tooltip" title="Add Main Category">Add Main Category</span></a>
									<div class="modal fade" id="catAdd_par" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Add main Category</h4>
												</div>
												<form action="" method="post">
												<div class="modal-body">
													<input type="hidden" name="par_id" value="0">
													<div class="form-group">
														<input type="text" name="cat_name" class="form-control" value="">
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
													<button type="submit" name="btn_catadd" value="catadd" class="btn btn-danger">
														Add
													</button>
												</div>
												</form>
											</div>
										</div>
									</div>

								</td>
								<td></td>
								<td></td>
							</tr>
							-->
						</table>

					</div><!-- /.box-body -->
					<div class="box-footer">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
