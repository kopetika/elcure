<p>&nbsp;</p>
        <!-- Page Content -->
        <section class="py-5">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center" style="color: #0d73b7;"><?=$cat_slug['name']?></h2>
                    </div>
                    <div class="col-md-12">
                        <hr class="style19">
                    </div>
                </div>
                <div class="row bg-mbk-grey">
                    <div class="col-md-12">
                        <p></p>
                        <p><?=$cat_slug['cat_desc']?></p>
                    </div>
                    <?php foreach ($cat_sub as $cat_sub_list): ?>
                        <?php
                            if ($cat_sub_list['cat_img'] == 'noimg.jpg')
                            {
                                $cat_sub_img = 'slide_1900x1080.png';
                            }
                            else
                            {
                                $cat_sub_img = $cat_sub_list['cat_img'];
                            }

                        ?>
                        <div class="col-md-6">
                            <img class="img-fluid mx-auto d-block all-radius" src="<?=base_url()?>/user_upload/<?=$cat_sub_img?>">
                            <p></p>
                            <h4 style="color: #0d73b7;">
                                <a href="<?=site_url()?>/content/mod/project/<?=$cat_sub_list['slug']?>">
                                    <?=$cat_sub_list['name']?>
                                </a>
                            </h4>
                            <p><?=$cat_sub_list['cat_desc']?></p>
                        </div>
                    <?php endforeach; ?>

                    <?php foreach ($mod_type as $mod_type_list): ?>
                        <?php
                            if ($mod_type_list['img'] == 'noimg.jpg')
                            {
                                $mod_img = 'slide_1900x1080.png';
                            }
                            else
                            {
                                $mod_img = $mod_type_list['img'];
                            }

                        ?>
                        <div class="col-md-6">
                            <img class="img-fluid mx-auto d-block all-radius" src="<?=base_url()?>/user_upload/<?=$mod_img?>">
                            <p></p>
                            <h4 style="color: #0d73b7;">
                                <a href="<?=site_url()?>/content/view/<?=$mod_type_list['slug']?>">
                                    <?=$mod_type_list['title']?>
                                </a>
                            </h4>
                            <p><?=strip_tags(substr($mod_type_list['content'], 0, 40))?></p>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </section>
