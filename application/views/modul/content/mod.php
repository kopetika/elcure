<?php
    if (isset($cat_slug))
    {
        $cat_title = $cat_slug['name'];
        $cat_desc  = $cat_slug['cat_desc'];
    }
    else
    {
        $cat_title = $this->uri->segment(3);
        $cat_desc  = '';
    }
?>
<style>
    .img-thumb{
        height: 150px;
        object-fit: cover;
    }
    .card-title{ min-height: 73px;}
    .card-text{ min-height: 50px;}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h2 class="mt-2" style="text-transform:capitalize; font-weight:100;"><?=$cat_title?></h2>
            <p><?=$cat_desc?></p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <?php foreach ($mod_type as $mod_type_list): ?>
            <div class="col-md-3" style="padding-bottom: 20px;">
                <div class="card shadow-lg">
                    <img src="<?=base_url()?>user_upload/<?=$mod_type_list['img']?>" class="card-img-top img-thumb" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?=$mod_type_list['title']?></h5>
                        <p class="card-text"><?=strip_tags(substr($mod_type_list['content'], 0, 60))?></p>
                        <a href="<?=site_url()?>/content/view/<?=$mod_type_list['slug']?>" class="btn btn-primary">more...</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>