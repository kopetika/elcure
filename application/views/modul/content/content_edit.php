<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Content
			<small>Content Manager</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url()?>/content_admin/content/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>"><?=$this->uri->segment(3)?></a></li>
			<li class="active">Edit</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<?php include('content_manage_menu.php'); ?>

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit <?=$this->uri->segment(3)?></h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">

				<?php
					if ($content_edit['content_pos'] == 'project-item')
					{
						include ('content_edit_project.php');
					}
					elseif ($content_edit['content_pos'] == 'slideshow-item')
					{
						include ('content_edit_slideshow.php');
					}
					elseif ($content_edit['content_pos'] == 'galery-item')
					{
						include ('content_edit_slideshow.php');
					}
					else
					{
						include ('content_edit_general.php');
					}
				?>

			</div><!-- /.box-body -->
			<div class="box-footer">
				&nbsp;
			</div><!-- /.box-footer-->
		</div><!-- /.box -->

	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
