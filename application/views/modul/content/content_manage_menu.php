	<div class="box box-info">
	    <div class="box-body">
	        <p></p>
	        <a href="<?=site_url()?>/content_admin/content/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>" class="btn btn-app">
	            <i class="fa fa-home"></i> List <?=$this->uri->segment(3)?>
	        </a>
	        <a href="<?=site_url()?>/content_admin/content_add/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>" class="btn btn-app">
	            <i class="fa fa-edit"></i> Add new <?=$this->uri->segment(3)?>
	        </a>
			<?php if ($this->uri->segment(3) == 'project'): ?>
				<a href="<?=site_url()?>/content_admin/content_category/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>" class="btn btn-app">
		            <i class="fa fa-sitemap"></i> Category
		        </a>
			<?php endif; ?>
	    </div>
	</div>
