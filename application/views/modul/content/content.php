<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Content
			<small>Content Manager</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">List</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<?php include('content_manage_menu.php'); ?>

		<?php
			if (isset($_POST['btn_content_debug']))
			{
				echo "<pre>";
				print_r($_POST);
				echo "</pre>";
			}
		?>

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">List <?=$this->uri->segment(3)?></h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<tr>
						<th>Menu / Title</th>
						<th>Category</th>
						<th>Content Position</th>
						<th></th>
						<th></th>
					</tr>
					<?php foreach ($content_list as $content): ?>
					<?php $data['content_cat']	= $this->content_admin_model->content_kategori_single($content['content_kategori_id']); ?>
					<?php $data['content_sub1']	= $this->content_admin_model->db_content_par_nolimit($content['id']); ?>
					<?php $category = $data['content_cat'] != null ? $data['content_cat']['name'] : '-'; ?>
					<tr>
						<td><?=$content['menu']?></td>
						<td><?=$category?></td>
						<td><?=$content['content_pos']?></td>
						<td>
							<a href="<?=site_url()?>/content/view/<?=$content['slug']?>" target="_blank" title="">Link</a>
						</td>
						<td nowrap>

							<a href="<?=site_url()?>/content_admin/content_edit/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content['id']?>" data-toggle="tooltip" title="Edit Content"><span class="glyphicon glyphicon-edit"></a>
							<a href="<?=site_url()?>/content_admin/content_foto/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content['id']?>" data-toggle="tooltip" title="Edit Image"><span class="glyphicon glyphicon-picture text-green"></a>

							<a href="#" data-toggle="modal" data-target="#contentDelete_<?=$content['id']?>" <?=$css_hidden?>><span class="glyphicon glyphicon-trash text-red" data-toggle="tooltip" title="Delete content"></span></a>
							<!-- Modal -->
							<div class="modal fade" id="contentDelete_<?=$content['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Delete Content</h4>
										</div>
										<form action="" method="post">
										<div class="modal-body">
											<input type="hidden" name="id" value="<?=$content['id']?>">
											<p>Are you sure want to delete this content?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<button type="submit" name="btn_delete" value="delete" class="btn btn-danger">
												Delete
											</button>
										</div>
										</form>
									</div>
								</div>
							</div>

						</td>
					</tr>
						<?php foreach ($data['content_sub1'] as $content_sub1): ?>
						<?php $data['content_cat1']	= $this->content_admin_model->content_kategori_single($content_sub1['content_kategori_id']); ?>
						<?php $category1 = $data['content_cat1'] != null ? $data['content_cat1']['name'] : '-'; ?>
							<tr>
								<td style="padding-left: 30px;"><?=$content_sub1['menu']?></td>
								<td><?=$category1?></td>
								<td><?=$content_sub1['content_pos']?></td>
								<td>
									<a href="<?=site_url()?>/content/view/<?=$content_sub1['slug']?>" target="_blank" title="">Link</a>
								</td>
								<td>

									<a href="<?=site_url()?>/content_admin/content_edit/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content_sub1['id']?>" data-toggle="tooltip" title="Edit Content"><span class="glyphicon glyphicon-edit"></a>
									<a href="<?=site_url()?>/content_admin/content_foto/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>/<?=$content_sub1['id']?>" data-toggle="tooltip" title="Edit Image"><span class="glyphicon glyphicon-picture text-green"></a>

									<a href="#" data-toggle="modal" data-target="#contentDelete_<?=$content_sub1['id']?>" <?=$css_hidden?>><span class="glyphicon glyphicon-trash text-red" data-toggle="tooltip" title="Delete content"></span></a>
									<!-- Modal -->
									<div class="modal fade" id="contentDelete_<?=$content_sub1['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Delete Content</h4>
												</div>
												<form action="" method="post">
												<div class="modal-body">
													<input type="hidden" name="id" value="<?=$content_sub1['id']?>">
													<p>Are you sure want to delete this content?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
													<button type="submit" name="btn_delete" value="delete" class="btn btn-danger">
														Delete
													</button>
												</div>
												</form>
											</div>
										</div>
									</div>

								</td>
							</tr>
						<?php endforeach ?>
					<?php endforeach ?>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer">
				&nbsp;<?php echo $paging; ?>
			</div><!-- /.box-footer-->
		</div><!-- /.box -->

	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
