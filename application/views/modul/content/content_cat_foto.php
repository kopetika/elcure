		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Content
					<small>Content Manager</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="<?=site_url()?>/content_admin/content/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>"><?=$this->uri->segment(3)?></a></li>
					<li><a href="<?=site_url()?>/content_admin/content_category/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>">category</a></li>
					<li class="active">image</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<?php if ($notif_photo_upload == '1'): ?>
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-info"></i> Success!</h4>
						Upload file successfully.
					</div>
				<?php elseif ($notif_photo_upload == '2'): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-info"></i> Warning!</h4>
						<?=$error?>
					</div>
				<?php endif; ?>

				<?php include('content_manage_menu.php'); ?>

				<?php
					//echo "<pre>";
					//echo print_r($_POST);
					//echo print_r($_FILES['userfile']);
					//echo "</pre>";
				?>

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"><?=$cek_content['name']?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">

							<table class="table table-hover">
								<tr>
									<th>Image</th>
									<th>Upload</th>
									<th></th>
								</tr>
								<tr>
									<td>
										<img src="<?=base_url()?>/user_upload/<?=$cek_content['cat_img']?>" class="img-thumbnail">
									</td>
									<td>
										<form role="form" method="post" id="form_img_cont" action="" enctype="multipart/form-data">
		                                    <div class="form-group">
		                                        <input type="file" name="userfile" onchange="document.getElementById('form_img_cont').submit()" required>
												<input type="hidden" name="field_val" value="cat_img">
												<?php
													if ($cek_content['id'] == 1)
													{
														$best_dim = '600x450 px';
													}
													elseif ($cek_content['id'] == 2)
													{
														$best_dim = '300x189 px';
													}
													elseif ($cek_content['id'] == 3)
													{
														$best_dim = '300x189 px';
													}
													elseif ($cek_content['id'] == 4)
													{
														$best_dim = '300x189 px';
													}
													elseif ($cek_content['id'] == 5)
													{
														$best_dim = '300x189 px';
													}
													elseif ($cek_content['id'] == 6)
													{
														$best_dim = '600x450 px';
													}
													else
													{
														$best_dim = '600x450 px';
													}
												?>
												<p class="help-block">Untuk hasil terbaik, dimensi gambar: <?=$best_dim?></p>
		                                    </div>
		                                </form>
									</td>
									<td>
										<form role="form" method="post" id="" action="">
		                                    <div class="form-group">
		                                        <input type="hidden" name="foto" value="noimg.jpg">
												<input type="hidden" name="field_val_del" value="cat_img">
												<button type="submit" name="btn_delete_img" value="delete_img" class="btn btn-warning">Delete</button>
		                                    </div>
		                                </form>
									</td>
								</tr>
							</table>

					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
