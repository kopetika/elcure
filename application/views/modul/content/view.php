<div class="container">
    <div class="row">
        <div class="col-md-12 text-center shadow-lg p-0 mb-5 bg-body rounded">
            <img src="<?=base_url()?>user_upload/<?=$content['img']?>" style="width:100%;">
            <h2 class="mt-4 mb-4" style="text-transform:capitalize; font-weight:100;"><?=$content['title']?></h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 shadow-lg p-5 mb-5 bg-body rounded">
            <p><?=$content['content']?></p>
        </div>
    </div>
</div>
