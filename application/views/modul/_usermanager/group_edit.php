		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Group
					<small>halaman pengelolaan User Group</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/group">Manage Group</a></li>
					<li class="active">Edit group</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Edit group developer</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">

						<?php
							if (!null == validation_errors())
							{
								echo "<div class='callout callout-danger'>";
								echo validation_errors();
								echo "</div>";
							}
							
							if ($sukses == '1')
							{
								echo "<div class='callout callout-info'>";
								echo "Edit detail group developer berhasil";
								echo "</div>";
								
								$cancel	= "Back";
							}
							else
							{
								$cancel	= "Cancel";
							}
						?>

						<div class="col-md-6">
							<input type="hidden" name="id" class="form-control" id="inputSuccess" value="<?=$this->uri->segment(4)?>">
							<div class="form-group has-success">
								<label class="control-label" for="inputError">Nama</label>
								<input type="text" name="nama" class="form-control" id="inputError" value="<?=$group['name']?>">
							</div>
						</div>

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
					
						<div class="col-md-6">
							<a href="<?=site_url()?>/admin/group" class="btn btn-default"><?=$cancel?></a>
							<button type="submit" name="group_edit_btn" value="group_edit" class="btn btn-info pull-right">Submit</button>
						</div>
						
					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->
				
			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->