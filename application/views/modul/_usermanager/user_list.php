		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Admin
					<small>halaman pengelolaan User Admin</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li class="active">Manage Admin</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Admin List</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th></th>
								<th>Nama</th>
								<th>Username</th>
								<th>Group</th>
								<th></th>
							</tr>
							<?php foreach ($admin_list as $admin): ?>
								<?php $data['perm']	= $this->admin_model->get_group_name($admin['permission']); ?>
								<tr>
									<td>&nbsp;</td>
									<td><?=$admin['nama']?></td>
									<td><?=$admin['username']?></td>
									<td><?=$data['perm']['name']?></td>
									<td>
										<?php if ($admin['user_id'] == 1): ?>
											<a href="<?=site_url()?>/admin/user"><span class="glyphicon glyphicon-trash" style="color:grey;"></span></a>
										<?php else: ?>
											<a href="<?=site_url()?>/admin/user/delete/<?=$admin['user_id']?>" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-trash text-red"></span></a>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach ?>
						</table>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						&nbsp;<?php echo $paging; ?>
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

				<!-- User Add Box /Default box -->
				<div class="box collapsed-box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add New Admin</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Expand"><i class="fa fa-plus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group has-success">
								<label class="control-label" for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" id="username" placeholder="Nama" required>
							</div>
							<div class="form-group has-success">
								<label class="control-label" for="username">Username</label>
								<input type="email" name="username" class="form-control" id="username" placeholder="Email" required>
							</div>
								<div class="form-group has-success">
									<label class="control-label" for="password">Password</label>
									<input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
								</div>
							<div class="form-group has-warning">
								<label class="control-label" for="permission">User Group</label>
								<select name="permission" class="form-control select2" id="permission" style="width: 100%;" required>
									<option></option>
									<?php foreach ($permission as $permission_list): ?>
									<option value="<?=$permission_list['id_perm']?>"><?=$permission_list['name']?></option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						<div class="col-md-6">
							<button type="submit" name="user_add_btn" value="user_add" class="btn btn-info">Submit</button>
						</div>
					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
