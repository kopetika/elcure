		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage User
					<small>halaman pengelolaan User Developer</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/user">Manage Developer</a></li>
					<li class="active">Delete akun</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Hapus akun developer</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">

						<?php
							if (!null == validation_errors())
							{
								echo "<div class='callout callout-danger'>";
								echo validation_errors();
								echo "</div>";
							}

							if ($sukses == '1')
							{
								$submit	= "disabled";
								$cancel	= "Back";
								$class_cancel		= "btn btn-info";
								$class_form_group	= "form-group";
								$username	= "";

								echo "<div class='callout callout-info'>";
								echo "Akun developer berhasil dihapus";
								echo "</div>";
							}
							elseif ($sukses == '0')
							{
								$submit	= "";
								$cancel	= "Cancel";
								$class_cancel		= "btn btn-default";
								$class_form_group	= "form-group has-success";
								$data['user']	= $this->admin_model->get_user_single($this->uri->segment(4));
								$username	= $data['user']['username'];

								echo "<p>";
								echo "Akun ini akan dihapus. Anda yakin akan menghapus akun ini?";
								echo "</p>";
							}
						?>

						<div class="col-md-6">
							<div class="<?=$class_form_group?>">
								<label class="control-label" for="inputSuccess">Username</label>
								<input type="text" name="username_dsp" class="form-control" id="inputSuccess" value="<?=$username?>" disabled>
								<input type="hidden" name="username" class="form-control" id="inputSuccess" value="<?=$username?>">
							</div>
						</div>

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">

						<div class="col-md-6">
							<a href="<?=site_url()?>/admin/user" class="<?=$class_cancel?>"><?=$cancel?></a>
							<button type="submit" name="user_delete_btn" value="user_delete" class="btn btn-danger pull-right" <?=$submit?>>Delete</button>
						</div>

					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
