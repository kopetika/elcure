		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage User
					<small>halaman pengelolaan User Developer</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/user">Manage Developer</a></li>
					<li class="active">Edit akun</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Edit akun developer</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">

						<?php
							if (!null == validation_errors())
							{
								echo "<div class='callout callout-danger'>";
								echo validation_errors();
								echo "</div>";
							}
							
							if ($sukses == '1')
							{
								echo "<div class='callout callout-info'>";
								echo "Edit detail akun developer berhasil";
								echo "</div>";
							}
						?>

						<div class="col-md-6">
							<div class="form-group has-success">
								<label class="control-label" for="inputSuccess">Username</label>
								<input type="text" name="username" class="form-control" id="inputSuccess" value="<?=$this->uri->segment(4);?>" disabled>
							</div>
							<div class="form-group has-success">
								<label class="control-label" for="inputError">Nama</label>
								<input type="text" name="nama" class="form-control" id="inputError" value="<?=$user['nama']?>">
							</div>
							<div class="form-group has-error">
								<label class="control-label" for="inputError">Email</label>
								<input type="email" name="email" class="form-control" id="inputError" value="<?=$user['email']?>">
							</div>
							<div class="form-group has-success">
								<label class="control-label" for="inputError">Telpon</label>
								<input type="text" name="telpon" class="form-control" id="inputError" value="<?=$user['telp']?>">
							</div>
						</div>

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
					
						<div class="col-md-6">
							<a href="<?=site_url()?>/admin/user" class="btn btn-default">Cancel</a>
							<button type="submit" name="user_edit_btn" value="user_edit" class="btn btn-info pull-right">Submit</button>
						</div>
						
					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->
				
			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->