		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Group
					<small>halaman pengelolaan User Group</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li class="active">Manage Group</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Group List</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th></th>
								<th>Group</th>
								<th></th>
							</tr>
							<?php foreach ($group_list as $group): ?>
							<?php
								$url_css_admin = ($group['id_perm'] == 1) ? 'disabled' : '';
								$url_css_partner = ($group['name'] == 'Partnership') ? 'disabled' : '';
							?>
							<tr>
								<td>&nbsp;</td>
								<td><?=$group['name']?></td>
								<td>
									<a href="<?=site_url()?>/admin/group/menu/<?=$group['id_perm']?>" class="btn btn-xs" data-toggle="tooltip" title="Set Group Menu"><span class="glyphicon glyphicon-menu-hamburger text-yellow"></span></a>
									<a href="<?=site_url()?>/admin/group/edit/<?=$group['id_perm']?>" class="btn btn-xs <?=$url_css_admin?> <?=$url_css_partner?>" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-pencil text-aqua"></span></a>
									<a href="<?=site_url()?>/admin/group/delete/<?=$group['id_perm']?>" class="btn btn-xs <?=$url_css_admin?> <?=$url_css_partner?>" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-trash text-red"></span></a>
								</td>
							</tr>
							<?php endforeach ?>
						</table>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						&nbsp;<?php echo $paging; ?>
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

				<!-- User Add Box /Default box -->
				<div class="box collapsed-box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add New Group</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Expand"><i class="fa fa-plus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group has-success">
								<label class="control-label" for="inputSuccess">Group Name</label>
								<input type="text" name="name" class="form-control" id="inputSuccess" placeholder="Enter Group Name" required>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						<div class="col-md-6">
							<button type="submit" name="group_add_btn" value="group_add" class="btn btn-info">Submit</button>
						</div>
					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
