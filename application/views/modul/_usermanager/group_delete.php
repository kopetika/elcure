		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Group
					<small>halaman pengelolaan User Group</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/group">Manage Group</a></li>
					<li class="active">Delete group</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Hapus group developer</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">

						<?php
							if (!null == validation_errors())
							{
								echo "<div class='callout callout-danger'>";
								echo validation_errors();
								echo "</div>";
							}
							
							if ($sukses == '1')
							{
								$submit	= "disabled";
								$cancel	= "Back";
								$class_cancel		= "btn btn-info";
								$class_form_group	= "form-group";
								
								echo "<div class='callout callout-info'>";
								echo "Group developer berhasil dihapus";
								echo "</div>";
							}
							elseif ($sukses == '0')
							{
								$submit	= "";
								$cancel	= "Cancel";
								$class_cancel		= "btn btn-default";
								$class_form_group	= "form-group has-success";
								
								echo "<p>";
								echo "Group ini akan dihapus. Anda yakin akan menghapus group ini?";
								echo "</p>";
							}
						?>
						
						<div class="col-md-6">
							<input type="hidden" name="id" class="form-control" id="inputSuccess" value="<?=$this->uri->segment(4)?>">
							<div class="<?=$class_form_group?>">
								<label class="control-label" for="inputSuccess">Group</label>
								<input type="text" name="nama" class="form-control" id="inputSuccess" value="<?=$group['name']?>" disabled>
							</div>
						</div>

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
					
						<div class="col-md-6">
							<a href="<?=site_url()?>/admin/group" class="<?=$class_cancel?>"><?=$cancel?></a>
							<button type="submit" name="group_delete_btn" value="group_delete" class="btn btn-danger pull-right" <?=$submit?>>Delete</button>
						</div>
						
					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->
				
			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->