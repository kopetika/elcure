		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Group
					<small>halaman pengelolaan User Group</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/group">Manage Group</a></li>
					<li class="active">Group Menu</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"><?=$group_name['name']?> Menu List</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body no-padding">

						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1" data-toggle="tab">Menu Aktif</a></li>
								<li><a href="#tab_2" data-toggle="tab">Add Menu</a></li>
							</ul>
							<div class="tab-content">
								<div class="table-responsive tab-pane active" id="tab_1">

									<table class="table table-hover">
										<tr>
											<th></th>
											<th>Menu</th>
											<th></th>
										</tr>
										<?php foreach ($group_menu as $menu): ?>
										<tr>
											<td>&nbsp;</td>
											<td><?=$menu['title']?></td>
											<td>
												<a href="<?=site_url()?>/admin/group/menu/<?=$this->uri->segment(4)?>/delete/<?=$menu['id_menu']?>" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-trash text-red"></span></a>
											</td>
										</tr>
										<?php endforeach ?>
									</table>

								</div><!-- /.tab-pane -->
								<div class="tab-pane clearfix" id="tab_2">

									<div class="col-md-6">
										<form method="post" action="" class="">
										<input type="hidden" name="id_perm" value="<?=$this->uri->segment(4)?>">
										<div class="form-group has-warning">
											<label class="control-label" for="inputWarning">Tambahkan hak akses menu ke dalam group</label>
											<select name="id_menu" class="form-control select2" style="width: 100%;" required>
												<option></option>
												<?php foreach ($menu_tg as $menu_list): ?>
												<option value="<?=$menu_list['id']?>"><?=$menu_list['title']?></option>
												<?php endforeach ?>
											</select>
										</div>
										<button type="submit" name="add_menu_btn" value="add_menu" class="btn btn-info"><span class="glyphicon glyphicon-chevron-left text-red"></span> Add</button>
										</form>
									</div>

								</div><!-- /.tab-pane -->
							</div><!-- /.tab-content -->
						</div><!-- nav-tabs-custom -->

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
