		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Manage Group
					<small>halaman pengelolaan User Group</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=site_url()?>/admin/group">Manage Group</a></li>
					<li><a href="<?=site_url()?>/admin/group/menu/<?=$this->uri->segment(4)?>">Group Menu</a></li>
					<li class="active">Delete Menu</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- User List Box /Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Hapus menu untuk group</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form role="form" method="post" action="">
					<div class="box-body">

						<?php
							if ($notif_group_menu_delete == '1')
							{
								$submit	= "disabled";
								$cancel	= "Back";
								$class_cancel		= "btn btn-info";
								$class_form_group	= "form-group";

								echo "<div class='callout callout-info'>";
								echo "Menu berhasil dihapus dari Group.";
								echo "</div>";
							}
							elseif ($notif_group_menu_delete == '0')
							{
								$submit	= "";
								$cancel	= "Cancel";
								$class_cancel		= "btn btn-default";
								$class_form_group	= "form-group has-success";

								echo "<p>";
								echo "Menu untuk Group ini akan dihapus. Anda yakin akan menghapus menu ini?";
								echo "</p>";
							}
						?>

						<div class="col-md-6">
							<input type="hidden" name="id" value="<?=$this->uri->segment(6)?>">
							<div class="<?=$class_form_group?>">
								<label class="control-label" for="inputSuccess">Menu</label>
								<input type="text" name="nama" class="form-control" id="inputSuccess" value="<?=$menu_group_delete['title']?>" disabled>
							</div>
						</div>

					</div><!-- /.box-body -->
					<div class="box-footer clearfix">

						<div class="col-md-6">
							<a href="<?=site_url()?>/admin/group/menu/<?=$this->uri->segment(4)?>" class="<?=$class_cancel?>"><?=$cancel?></a>
							<button type="submit" name="menu_group_delete_btn" value="menu_group_delete" class="btn btn-danger pull-right" <?=$submit?>>Delete Menu</button>
						</div>

					</div><!-- /.box-footer-->
					</form>
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
