<?php $data['product']	= $this->content_model->get_content_type_cat('product', 0); ?>
<?php $data['product']	= $this->content_model->get_content_type_cat('product', 0); ?>
<?php $data['dukungan']	= $this->content_model->get_content_type_cat('dukungan', 0); ?>
<?php $data['about']	= $this->content_model->get_content_type_cat('about', 0); ?>
<?php $data['partner']	= $this->content_model->get_content_type_cat('partner', 0); ?>
		<!-- ============= COMPONENT ============== -->
		<nav class="navbar navbar-expand-lg navbar-light bg-default">
			<div class="container-fluid">
				<a class="navbar-brand" href="<?=base_url()?>" title="Elcure International">
					<img style="display: unset; width: 150px;" class="img-responsive" src="<?=base_url()?>user_upload/LOGO_ELCURE_Transparan_border.png" alt="">
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav"  aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="main_nav">
					<ul class="navbar-nav">
						<li class="nav-item active"> <a class="nav-link" href="<?=base_url()?>"><i class="bi bi-house-door-fill"></i></a> </li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Berita / Artikel</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<div class="col-lg-3 col-6">
										<div class="col-megamenu">
											<h6 class="title"><a href="<?=site_url()?>/content/mod/artikel">Artikel</a></h6>
										</div>
									</div>
									<div class="col-lg-3 col-6">
										<div class="col-megamenu">
											<h6 class="title"><a href="<?=site_url()?>/content/mod/news">Elcure News</a></h6>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Produk</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<?php foreach ($data['product'] as $product): ?>
										<?php $data['cek_subproduct'] = $this->content_model->count_sub_menu($product['id']); ?>
										<div class="col-lg-3 col-6">
											<div class="col-megamenu">
												<h6 class="title"><a href="<?=site_url()?>/content/view/<?=$product['slug']?>"><?=$product['title']?></a></h6>
												<?php if ($data['cek_subproduct'] > 0) : ?>
												<?php $data['subproduct'] = $this->content_model->get_content_type_cat('product', $product['id']); ?>
												<ul class="list-unstyled">
													<?php foreach ($data['subproduct'] as $subproduct): ?>
														<li><a href="<?=site_url()?>/content/view/<?=$subproduct['slug']?>"><?=$subproduct['title']?></a></li>
													<?php endforeach; ?>
												</ul>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?=site_url()?>/content/promo">Promo Elcure</a></li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Dukungan Online</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<?php foreach ($data['dukungan'] as $dukungan): ?>
										<?php $data['cek_subdukungan'] = $this->content_model->count_sub_menu($dukungan['id']); ?>
										<div class="col-lg-3 col-6">
											<div class="col-megamenu">
												<h6 class="title"><a href="<?=site_url()?>/content/view/<?=$dukungan['slug']?>"><?=$dukungan['title']?></a></h6>
												<?php if ($data['cek_subdukungan'] > 0) : ?>
												<?php $data['subdukungan'] = $this->content_model->get_content_type_cat('dukungan', $dukungan['id']); ?>
												<ul class="list-unstyled">
													<?php foreach ($data['subdukungan'] as $subdukungan): ?>
														<li><a href="<?=site_url()?>/content/view/<?=$subdukungan['slug']?>"><?=$subdukungan['title']?></a></li>
													<?php endforeach; ?>
												</ul>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Info Perusahaan</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<?php foreach ($data['about'] as $about): ?>
										<?php $data['cek_subabout'] = $this->content_model->count_sub_menu($about['id']); ?>
										<div class="col-lg-3 col-6">
											<div class="col-megamenu">
												<h6 class="title"><a href="<?=site_url()?>/content/view/<?=$about['slug']?>"><?=$about['title']?></a></h6>
												<?php if ($data['cek_subabout'] > 0) : ?>
												<?php $data['subabout'] = $this->content_model->get_content_type_cat('about', $about['id']); ?>
												<ul class="list-unstyled">
													<?php foreach ($data['subabout'] as $subabout): ?>
														<li><a href="<?=site_url()?>/content/view/<?=$subabout['slug']?>"><?=$subabout['title']?></a></li>
													<?php endforeach; ?>
												</ul>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Dokumentasi</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<div class="col-lg-3 col-6">
										<div class="col-megamenu">
											<h6 class="title"><a href="<?=site_url()?>/content/video">Video</a></h6>
										</div>
									</div>
									<div class="col-lg-3 col-6">
										<div class="col-megamenu">
											<h6 class="title"><a href="<?=site_url()?>/content/galery">Gallery Foto</a></h6>
										</div>
									</div>
									<div class="col-lg-3 col-6">
										<div class="col-megamenu">
											<h6 class="title"><a href="<?=site_url()?>/content/view/kesaksian-produk-elcure">Kesaksian Produk Elcure</a></h6>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown has-megamenu">
							<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Partner Bisnis</a>
							<div class="dropdown-menu megamenu" role="menu" style="border-top-left-radius:0px; border-top-right-radius:0px;">
								<div class="row g-3">
									<?php foreach ($data['partner'] as $partner): ?>
										<?php $data['cek_subpartner'] = $this->content_model->count_sub_menu($partner['id']); ?>
										<div class="col-lg-3 col-6">
											<div class="col-megamenu">
												<h6 class="title"><a href="<?=site_url()?>/content/view/<?=$partner['slug']?>"><?=$partner['title']?></a></h6>
												<?php if ($data['cek_subpartner'] > 0) : ?>
												<?php $data['subpartner'] = $this->content_model->get_content_type_cat('about', $about['id']); ?>
												<ul class="list-unstyled">
													<?php foreach ($data['subpartner'] as $subpartner): ?>
														<li><a href="<?=site_url()?>/content/view/<?=$subpartner['slug']?>"><?=$subpartner['title']?></a></li>
													<?php endforeach; ?>
												</ul>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?=site_url()?>/content/view/kontak">Kontak</a></li>
					</ul>
					<ul class="navbar-nav ms-auto">
						<li class="nav-item"><a href="<?=site_url()?>/admin/login" class="nav-link" href="#"><i class="bi bi-person-bounding-box"></i></a></li>
						<li class="nav-item"><div id="google_translate_element"></div></li>
					</ul>
				</div> <!-- navbar-collapse.// -->
			</div> <!-- container-fluid.// -->
		</nav>
		<!-- ============= COMPONENT END// ============== -->


<script type="text/javascript">
function googleTranslateElementInit() {
	new google.translate.TranslateElement({pageLanguage: 'id'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>