<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="keywords" content="html, css bootstrap, mega menu, navbar, large dropdown, menu CSS examples" />
	<meta name="description" content="Bootstrap 5 navbar megamenu examples with simple css demo code" />  

	<title>Elcure Indonesia</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

    <style type="text/css">
    h1, h2, h3, h4 {font-weight: 100;}
    a{text-decoration: none;}
    .navbar{ font-size: 13px; }
    .navbar .megamenu{ padding: 1rem; }
    .megamenu{ border: 0px; }

    blockquote {
        font-family: Georgia;
        color: #a1a1a1;
        letter-spacing: 1px;
        margin: 15px 0px 25px 20px;
        padding-left: 10px;
        border-left: 5px solid #c1c1c1;
    }
    
    .has-megamenu a {
        color: #666666;
    }

    .col-megamenu h6 {
        margin-bottom: 20px;
    }

    .col-megamenu li {
        font-size: 13px;
        margin-bottom: 10px;
    }

    .col-megamenu li a:hover {
        font-weight: 500;
    }

    .heading-block{
        padding-top: 90px;
        padding-bottom: 50px;
    }

    .videowrapper { float: none; clear: both; width: 100%; position: relative; padding-bottom: 56.25%; padding-top: 25px; height: 0; }
    .videowrapper iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }

    .block-news {
        position: relative;
        /* max-width: 800px; Maximum width */
        margin: 0 auto; /* Center it */
    }

    .block-news .block-news-item {
        position: absolute; /* Position the background text */
        bottom: 0; /* At the bottom. Use top:0 to append it to the top */
        background: rgb(0, 0, 0); /* Fallback color */
        background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
        color: #f1f1f1; /* Grey text */
        width: 100%; /* Full width */
        padding: 20px; /* Some padding */
    }

    footer {
        font-size: 12px;
        padding: 40px 30px;
    }

    /* ============ desktop view ============ */
    @media all and (min-width: 992px) {
        
        .navbar .has-megamenu{position:static!important;}
        .navbar .megamenu{left:0; right:0; width:100%; margin-top:0;  }
        
    }	
    /* ============ desktop view .end// ============ */

    /* ============ mobile view ============ */
    @media(max-width: 991px){
        .navbar.fixed-top .navbar-collapse, .navbar.sticky-top .navbar-collapse{
            overflow-y: auto;
            max-height: 90vh;
            margin-top:10px;
        }
    }
    /* ============ mobile view .end// ============ */
    </style>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            /////// Prevent closing from click inside dropdown
            document.querySelectorAll('.dropdown-menu').forEach(function(element){
                element.addEventListener('click', function (e) {
                    e.stopPropagation();
                });
            })
        }); 
        // DOMContentLoaded  end
    </script>

</head>
<body>

	<main>