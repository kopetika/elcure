<style>
    .img-promo{
        height: 400px;
        object-fit: cover;
    }

    .img-news{
        height: 450px;
        object-fit: cover;
		object-position: 50% 100%;
    }
</style>

<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
	<div class="carousel-indicators">
		<?php $urut = 0; ?>
		<?php foreach ($slideshow as $slideshow_indikator): ?>
		<button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="<?=$urut?>" class="<?php if ($urut == 0){ echo "active";} ?>" aria-current="true" aria-label="Slide 1"></button>
		<?php $urut++; ?>
		<?php endforeach; ?>
	</div>
	<div class="carousel-inner">
		<?php $urut_s = 0; ?>
		<?php foreach ($slideshow as $slide_show): ?>
		<div class="carousel-item <?php if ($urut_s == 0) { echo "active"; } ?>">
			<img src="<?=base_url()?>user_upload/<?=$slide_show['img']?>" class="d-block w-100" alt="...">
			<div class="carousel-caption d-none d-md-block">
			<h5><?=$slide_show['teks']?></h5>
			<p></p>
			</div>
		</div>
		<?php $urut_s++; ?>
		<?php endforeach; ?>
	</div>
	<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="visually-hidden">Previous</span>
	</button>
	<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="visually-hidden">Next</span>
	</button>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 text-center heading-block">
			<h2>Promo Terbaru</h2>
			<p>Lihat semua promo <a href="<?=site_url()?>/content/promo">disini</a></p>
		</div>
	</div>
	<div class="row justify-content-evenly">
		<?php foreach ($promo as $promo_list): ?>
			<div class="col-5">
				<img
				src="<?=base_url()?>user_upload/<?=$promo_list['img']?>"
				class="w-100 shadow-1-strong rounded mb-4 img-promo hover-shadow"
				alt=""
                />
			</div>
		<?php endforeach; ?>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 text-center heading-block">
			<h2>Video</h2>
			<p>Lihat semua video <a href="<?=site_url()?>/content/video">disini</a></p>
		</div>
	</div>
	<div class="row justify-content-evenly">
		<?php foreach ($video as $video_list): ?>
			<div class="col-3">
				<div class="videowrapper"><?=$video_list['content']?></div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<div class="container-fluid shadow-lg">
	<div class="row">
		<div class="col-md-12 text-center heading-block">
			<h2>Berita | Artikel</h2>
			<p>Lihat semua Berita <a href="<?=site_url()?>/content/mod/news">disini</a> | Lihat semua Artikel <a href="<?=site_url()?>/content/mod/artikel">disini</a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 p-0">
			<div class="block-news">
				<img src="<?=base_url()?>user_upload/<?=$latestNews[0]['img']?>" alt="Notebook" class="img-news">
				<div class="block-news-item">
					<span class="badge bg-danger">Berita</span>
					<h1><?=$latestNews[0]['title']?></h1>
					<p><a href="<?=site_url()?>/content/view/<?=$latestNews[0]['slug']?>">Lihat...</a></p>
				</div>
			</div>
		</div>
		<div class="col-md-6 p-0">
			<div class="block-news">
				<img src="<?=base_url()?>user_upload/<?=$latestArtikel[0]['img']?>" alt="Notebook" class="img-news">
				<div class="block-news-item">
					<span class="badge bg-warning">Berita</span>
					<h1><?=$latestArtikel[0]['title']?></h1>
					<p><a href="<?=site_url()?>/content/view/<?=$latestArtikel[0]['slug']?>">Lihat...</a></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 text-center heading-block">
			<h2>&nbsp;</h2>
			<p>&nbsp;</p>
		</div>
	</div>
</div>