<style media="screen">
.sembunyi {
	display: none;
}
</style>
<?php
	$disabled = "disabled";
	$finish = "disabled";

	$saklar = $this->uri->segment(3);

	if ($saklar == 'upload')
	{
		$data['outlet']	= $this->staf_model->get_outlet_single($this->uri->segment(4));

		$form_data = 'sembunyi';
		$form_upload = '';
		$sukses = "0";
	} else {
		$form_data = '';
		$form_upload = 'sembunyi';
	}

	if (!empty($data['outlet']['foto_outlet']) and !empty($data['outlet']['foto_bca_vir_acc']))
    {
        $finish = "";
    }

?>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>for all staff activity</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active"><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<?php
				if ($sukses == '1')
				{
					?>
					<div class='callout callout-danger'>
						<p>The phone number <strong><?=$phone_input?></strong> is already registered from <strong><?=$auth_phone['nama_store']?></strong></p>
						<p>You can not enter the same phone number.</p>
					</div>
					<?php
				}
				?>

				<?php if ($notif_outlet_upload == 1): ?>
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-info"></i> Success!</h4>
						Upload file successfully.
					</div>
				<?php endif; ?>

				<?php if ($notif_outlet_upload == 2): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-info"></i> Warning!</h4>
						<?=$error?>
					</div>
				<?php endif; ?>

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Input Outlet</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">
						<p class="pull-right"><?=date('Y-m-d')?></p>
						<div class="col-md-6">
							<form class="form-horizontal <?=$form_data?>" action="" method="post">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Area</label>
									<div class="col-sm-10">
										<select class="form-control" name="area" required>
											<option value="">-</option>
											<?php foreach ($area as $area_list): ?>
											<option value="<?=$area_list['id']?>"><?=$area_list['area']?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Owner</label>
									<div class="col-sm-10">
										<input type="text" name="owner_name" class="form-control" value="" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Nama Toko</label>
									<div class="col-sm-10">
										<input type="text" name="store_name" class="form-control" value="" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Phone</label>
									<div class="col-sm-10">
										<input type="text" name="store_phone" class="form-control" value="" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Address</label>
									<div class="col-sm-10">
										<textarea name="store_address" rows="5" cols="50" class="form-control" required></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Patokan</label>
									<div class="col-sm-10">
										<textarea name="patokan" rows="5" cols="50" class="form-control" required></textarea>
									</div>
								</div>
								<button type="submit" class="btn btn-info pull-right" name="btn_add_store" value="add_store">Next</button>
							</form>

							<form id="form_foto_ktp" class="form-horizontal <?=$form_upload?>" action="" method="post" enctype="multipart/form-data">
								<?php if (!empty($data['outlet']['foto_ktp'])): ?>
									<img class="img-thumbnail" src="<?=base_url()?>/user_upload/<?=$data['outlet']['foto_ktp']?>" alt="">
								<?php endif; ?>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Foto KTP</label>
									<div class="col-sm-9">
										<input type="hidden" name="foto_outlet_form" value="foto_ktp">
										<input type="file" name="userfile" onchange="document.getElementById('form_foto_ktp').submit()" value="">
									</div>
								</div>
							</form>

							<form id="form_foto_outlet" class="form-horizontal <?=$form_upload?>" action="" method="post" enctype="multipart/form-data">
								<?php if (!empty($data['outlet']['foto_outlet'])): ?>
									<img class="img-thumbnail" src="<?=base_url()?>/user_upload/<?=$data['outlet']['foto_outlet']?>" alt="">
								<?php endif; ?>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Foto Selfie</label>
									<div class="col-sm-9">
										<input type="hidden" name="foto_outlet_form" value="foto_outlet">
										<input type="file" name="userfile" onchange="document.getElementById('form_foto_outlet').submit()" value="">
									</div>
								</div>
							</form>

							<form id="form_bca_vir_acc" class="form-horizontal <?=$form_upload?>" action="" method="post" enctype="multipart/form-data">
								<?php if (!empty($data['outlet']['foto_bca_vir_acc'])): ?>
									<img class="img-thumbnail" src="<?=base_url()?>/user_upload/<?=$data['outlet']['foto_bca_vir_acc']?>" alt="">
								<?php endif; ?>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Foto Virtual Acc.</label>
									<div class="col-sm-9">
										<input type="hidden" name="foto_outlet_form" value="foto_bca_vir_acc">
										<input type="file" name="userfile" onchange="document.getElementById('form_bca_vir_acc').submit()" value="">
									</div>
								</div>
							</form>
							<a href="<?=site_url()?>/staf" class="btn btn-info <?=$form_upload?>" <?=$finish?>>Finish</a>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
