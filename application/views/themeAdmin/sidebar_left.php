		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?=base_url()?>user_upload/nofoto.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p><?=$this->session->nama?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<?php include('_inc_sidebarleft_search.php'); ?>
				<form action="" method="post" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="search" class="form-control" placeholder="<?=$placeholder?>" <?=$disabled?>>
						<span class="input-group-btn">
							<button type="submit" name="<?=$bnt_name?>" value="search_act" id="search-btn" class="btn btn-flat" <?=$disabled?>>
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul id="sidebar_left" class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li>
						<a href="<?=site_url()?>/admin">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						</a>
					</li>
					<?php foreach ($menu as $row_menu): ?>
						<li>
							<a href="<?=site_url()?><?=$row_menu['url']?>" <?=$row_menu['url_param']?>>
								<i class="fa fa-angle-right"></i>
								<span><?=$row_menu['title']?></span>
								<span class="pull-right-container"><?=$row_menu['right_container']?></span>
							</a>
						</li>
					<?php endforeach ?>
					<?php
					if ($this->session->permission == 1)
					{
					?>
						<li class="header">ADMIN MANAGER</li>
						<li><a href="<?=site_url()?>/admin/user"><i class="fa fa-user text-aqua"></i> <span>Manage Admin</span></a></li>
						<li><a href="<?=site_url()?>/admin/group"><i class="fa fa-users text-yellow"></i> <span>Manage Group</span></a></li>
						<li><a href="<?=site_url()?>/admin/logout"><i class="fa fa-power-off text-red"></i> <span>Sign Out</span></a></li>
					<?php
					}//EOF if ($this->session->auth_status == 3)
					?>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->
