    <div class="login-box">
		<div class="login-logo">
			<img src="<?=base_url()?>user_upload/LOGO_ELCURE_Transparan_border.png" alt="">
		</div><!-- /.login-logo -->
		<div class="login-box-body">

			<?php
				if ($sukses == '1')
				{
					echo "<div class='callout callout-danger'>";
					echo "Login tidak berhasil";
					echo "</div>";
				}
				elseif ($sukses == '2')
				{
					echo "<div class='callout callout-info'>";
					echo "Login berhasil";
					echo "</div>";

					$sys_login = array(
							'user_id'	=> $auth_login['user_id'],
                            'nama'      => $auth_login['nama'],
							'email'     => $auth_login['username'],
							'permission'	=> $auth_login['permission'],
							'is_admin'	=> TRUE
						);

					$this->session->set_userdata($sys_login);

					header('location:'.site_url().'/admin/');

				}
			?>

			<h3 class="login-box-msg">Login</h3>
			<form action="" method="post">
				<div class="form-group has-feedback">
					<input type="text" name="username" class="form-control" placeholder="Email" value="<?=set_value('username')?>" required>
					<span class="glyphicon glyphicon-option-vertical form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" value="<?=set_value('password')?>" required>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<p>&nbsp;</p>
					<div class="col-xs-8">
						<a href="<?=site_url()?>/bussiness/partner">Register</a>
					</div><!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" name="login_btn" value="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div><!-- /.col -->
				</div>
			</form>

			&nbsp;<br>
			&nbsp;

		</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
