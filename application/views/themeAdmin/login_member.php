<?php if ($this->uri->segment(3) == 'resetpass'): ?>

    <div class="login-box">
		<div class="login-logo">
			&nbsp;
		</div><!-- /.login-logo -->
		<div class="login-box-body">

            <h4>Reset Password</h4>
            <?php if ($notif_resetpass == 1): ?>
                <h5 style="color:red;">Email tidak terdaftar. Pastikan email Anda terdaftar di sistem kami.</h5>
            <?php elseif ($notif_resetpass == 2): ?>
                <h5 style="color:red;">Reset password berhasil. Silahkan periksa inbox email Anda.</h5>
            <?php else: ?>
                <h5>Password baru akan dikirimkan ke email Anda</h5>
            <?php endif; ?>

            <p class="login-box-msg"><?//=$message?></p>
			<form action="" method="post">
				<div class="form-group has-feedback">
					<input type="email" name="email" class="form-control" placeholder="Email" value="<?=set_value('email')?>" required>
					<span class="glyphicon glyphicon-option-vertical form-control-feedback"></span>
				</div>
				<div class="row">
					<p>&nbsp;</p>
					<div class="col-xs-8">
						Belum punya akun? <a href="<?=site_url()?>/member/daftar">Daftar disini</a><br />
                        <a href="<?=site_url()?>/member/login">Login</a><br />
                        <!--Belum menerima kode aktifasi? <a href="<?=site_url()?>/member/login/resend_aktivasi">Resend code</a>-->
					</div><!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" name="reset_pass_btn" value="reset_pass" class="btn btn-primary btn-block btn-flat">Reset</button>
					</div><!-- /.col -->
				</div>
			</form>

			&nbsp;<br>
			&nbsp;

		</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php elseif($this->uri->segment(3) == 'aktivasi'): ?>

    <div class="login-box">
		<div class="login-logo">
			&nbsp;
		</div><!-- /.login-logo -->
		<div class="login-box-body">

            <?php
                $actv_mail = $this->uri->segment(4);
                $actv_code = $this->uri->segment(5);

                $data['get_email'] = $this->member_model->get_email($actv_mail);

                if ($data['get_email']['activation_code'] == $actv_code)
                {
                    $this->member_model->member_aktivasi($actv_mail);
                    $text_activasi = 'Akun Anda telah aktif. Silahkan login disini:';
                }
                else
                {
                    $text_activasi = 'Kode tidak sesuai. Silahkan menghubungi perusahaan untuk mengaktifkan akun Anda.';
                }

            ?>

            <h5><?=$text_activasi?></h5>
            <a href="<?=site_url()?>/member/login">Login</a>
			&nbsp;<br>
			&nbsp;

		</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php elseif($this->uri->segment(3) == 'resend_aktivasi'): ?>

    <div class="login-box">
		<div class="login-logo">
			&nbsp;
		</div><!-- /.login-logo -->
		<div class="login-box-body">

            <h4>Resend Kode Aktivasi</h4>
            <?php if ($notif_resendcode == 1): ?>
                <h5 style="color:red;">Email tidak terdaftar. Pastikan email Anda terdaftar di sistem kami.</h5>
            <?php elseif ($notif_resendcode == 2): ?>
                <h5 style="color:red;">Kode aktifasi berhasil dikirimkan. Silahkan periksa inbox email Anda.</h5>
            <?php endif; ?>

            <p class="login-box-msg"><?//=$message?></p>
			<form action="" method="post">
				<div class="form-group has-feedback">
					<input type="email" name="email" class="form-control" placeholder="Email" value="<?=set_value('email')?>" required>
					<span class="glyphicon glyphicon-option-vertical form-control-feedback"></span>
				</div>
				<div class="row">
					<p>&nbsp;</p>
					<div class="col-xs-8">
						Belum punya akun? <a href="<?=site_url()?>/member/daftar">Daftar disini</a><br />
                        <a href="<?=site_url()?>/member/login">Login</a><br />
					</div><!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" name="resend_code_btn" value="resend_code" class="btn btn-primary btn-block btn-flat">Resend</button>
					</div><!-- /.col -->
				</div>
			</form>

			&nbsp;<br>
			&nbsp;

		</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php else: ?>

    <div class="login-box">
		<div class="login-logo">
			&nbsp;
		</div><!-- /.login-logo -->
		<div class="login-box-body">

			<?php
				if ($sukses == '1')
				{
					echo "<div class='callout callout-danger'>";
					echo "Login tidak berhasil";
					echo "</div>";
				}
				elseif ($sukses == '2')
				{
					echo "<div class='callout callout-info'>";
					echo "Login berhasil";
					echo "</div>";

					$sys_login = array(
							'id'        => $auth_login['id'],
							'fullname'  => $auth_login['fullname'],
                            'email'     => $auth_login['email'],
							'is_member'	=> TRUE
						);

					$this->session->set_userdata($sys_login);

					header('location:'.site_url().'/member');

				}
                elseif ($sukses == '3')
                {
                    echo "<div class='callout callout-danger'>";
					echo "Captcha tidak sesuai";
					echo "</div>";
                }
			?>

			<p class="login-box-msg">Member Login</p>
            <p class="login-box-msg"><?//=$status?></p>
            <p class="login-box-msg"><?//=$message?></p>
			<form action="" method="post">
				<div class="form-group has-feedback">
					<input type="email" name="email" class="form-control" placeholder="Email" value="<?=set_value('email')?>" required>
					<span class="glyphicon glyphicon-option-vertical form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" value="<?=set_value('password')?>" required>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="vercode" placeholder="Verification Code" maxlength="5" autocomplete="off" required style="width: 225px;" />
                    <button type="button" class="btn btn-flat" name="button" style="background-color:#44b69e; color:#FFF; font-size:14;"><strong><?=$captcha?></strong></button>
                </div>
                <!--<img src="<?=base_url()?>application/views/themeAdmin/captcha.php">-->

				<div class="row">
					<p>&nbsp;</p>
					<div class="col-xs-8">
						Belum punya akun? <a href="<?=site_url()?>/member/daftar">Daftar disini</a><br />
                        <a href="<?=site_url()?>/member/login/resetpass">Lupa password</a><br />
                        <!--Belum menerima kode aktifasi? <a href="<?=site_url()?>/member/login/resend_aktivasi">Resend code</a>-->
					</div><!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" name="staf_login_btn" value="staf_login" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div><!-- /.col -->
				</div>
			</form>

			&nbsp;<br>
			&nbsp;

		</div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php endif; ?>
