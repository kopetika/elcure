		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>it all starts here</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=site_url()?>/member"><i class="fa fa-dashboard"></i> Home</a></li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<?php foreach ($asuransi_paket as $asuransi_paket_list): ?>
					<?php $data['usia']	= $this->member_model->get_usia_member($m_info['birthdate'],$asuransi_paket_list['efektif_polis']); ?>
					<?php $data['asuransi_paket_produk']	= $this->asuransi_model->get_asuransi_paket_produk($asuransi_paket_list['id']); ?>

					<!-- Default box -->
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Paket <?=$asuransi_paket_list['paket']?></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body table-responsive">
							<?=$asuransi_paket_list['keterangan']?>
							<table class="table table-hover">
								<tr>
									<th>Produk</th>
									<th>Peserta</th>
									<th>Jenis Kelamin</th>
									<th>Usia Minimal</th>
									<th>Usia Maximal</th>
									<th>Premi Per-Tahun</th>
									<th>Efektif Polis Awal</th>
									<th>Efektif Polis Akhir</th>
									<th>Registrasi</th>
								</tr>
								<?php foreach ($data['asuransi_paket_produk'] as $asuransi_paket_produk): ?>
									<?php $data['cek_memberasuransi']	= $this->member_model->cek_memberasuransi_member($this->session->id); ?>
									<?php
										if ($asuransi_paket_produk['jenis'] == 'P')
										{
											$text_gender = 'PRIA';
										}
										elseif ($asuransi_paket_produk['jenis'] == 'W')
										{
											$text_gender = 'WANITA';
										}
										elseif ($asuransi_paket_produk['jenis'] == 'A')
										{
											$text_gender = 'ANAK';
										}
									?>
									<tr>
										<td><?=$asuransi_paket_produk['kode']?></td>
										<td><?=$asuransi_paket_produk['deskripsi']?></td>
										<td><?=$text_gender?></td>
										<td><?=$asuransi_paket_produk['min']?></td>
										<td><?=$asuransi_paket_produk['max']?></td>
										<td><?=number_format($asuransi_paket_produk['premi'],2,",",".")?></td>
										<td><?=$asuransi_paket_list['efektif_polis']?></td>
										<td><?=$asuransi_paket_list['efektif_polis_end']?></td>
										<td>
											<?php if ($data['cek_memberasuransi'] == 0): ?>
												<?php if ($asuransi_paket_produk['jenis'] == $m_info['gender']): ?>
													<?php if (($data['usia'] > $asuransi_paket_produk['min']) && ($data['usia'] <= $asuransi_paket_produk['max'])): ?>
														<form class="" action="<?=site_url()?>/member/index/daftar" method="post">
															<input type="hidden" name="id_produk" value="<?=$asuransi_paket_produk['id']?>">
															<button type="submit" name="reg_asuransi_btn" value="reg_asuransi" class="btn btn-danger pull-right">Daftar Program Asuransi</button>
														</form>
													<?php endif; ?>
												<?php endif; ?>
											<?php else: ?>
												<button type="button" name="button" class="btn btn-default" disabled>Anda tidak bisa memilih paket</button>
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</table>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				<?php endforeach; ?>

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->

		<script type="text/javascript">
		    function pop_up(){
		        alert("Untuk sementara waktu, pembayaran tunai tidak dapat dilaksanakan karena sedang dilakukan pemeliharaan sistem. Mohon maaf atas ketidaknyamanan ini. Salam.");
		    }
		</script>
