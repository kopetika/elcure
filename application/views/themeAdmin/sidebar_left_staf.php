		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?=base_url()?>user_upload/nofoto.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p><?=$this->session->fullname?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<?php include('_inc_sidebarleft_search.php'); ?>
				<form action="" method="post" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="search" class="form-control" placeholder="..." <?=$disabled?>>
						<span class="input-group-btn">
							<button type="submit" name="btn_serc" value="search_act" id="search-btn" class="btn btn-flat" <?=$disabled?>>
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul id="sidebar_left" class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li>
						<a href="<?=site_url()?>/member">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						</a>
					</li>
					<li>
						<a href="<?=site_url()?>/member/profile">
							<i class="fa fa-chevron-right"></i> <span>Profile</span>
						</a>
					</li>
					<li>
						<a href="<?=site_url()?>/member/program">
							<i class="fa fa-chevron-right"></i> <span>Pembelian</span>
						</a>
					</li>
					<li><a href="<?=site_url()?>/member/logout"><i class="fa fa-power-off text-red"></i> <span>Sign Out</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->
