    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script>
		$(function() {
			$('#sidebar_left a[href~="' + location.href + '"]').parents('li').addClass('active');
		});
	</script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>application/views/themeAdmin/bootstrap/js/bootstrap.min.js"></script>

	<!-- Select2 -->
	<script src="<?=base_url()?>application/views/themeAdmin/plugins/select2/select2.full.min.js"></script>
	<script>
		$(function () {
			//Initialize Select2 Elements
			$(".select2").select2();
		});
	</script>

	<!-- date-range-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="<?=base_url()?>application/views/themeAdmin/plugins/daterangepicker/daterangepicker.js"></script>
	<script>
		$(function () {
			$('#reservation').daterangepicker({
				format: 'YYYY-MM-DD'
			});
		});
	</script>

	<!-- bootstrap datepicker -->
	<script src="<?=base_url()?>application/views/themeAdmin/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script>
		$(function () {
			//Date picker 1
			$('#datepicker1').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});

			//Date picker 2
			$('#datepicker2').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});

			//Date picker 3
			$('#datepicker3').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});

			//Date picker 4
			$('#datepicker4').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});
		});
	</script>

    <!-- SlimScroll -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/fastclick/fastclick.min.js"></script>

    <!-- AdminLTE App -->
    <script src="<?=base_url()?>application/views/themeAdmin/dist/js/app.min.js"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>application/views/themeAdmin/dist/js/demo.js"></script>

    <!-- DataTables -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "order": [[ 7, "desc" ]],
                "info": true,
                "autoWidth": false
            });
            $("#example3").DataTable();
            $("#example4").DataTable({
                "order": [[ 7, "desc" ]]
            });
            $("#example5").DataTable({
                "order": [[ 3, "asc" ]]
            });
        });

    </script>

    <!-- bootstrap time picker -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script>
    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
            showMeridian: false,
            showInputs: false
        });
    });
    </script>

    <script src="<?=base_url()?>ckeditor/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url()?>application/views/themeAdmin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
        var roxyFileman = '<?=base_url()?>ckeditor/plugins/fileman/index.html';
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload',
            });
            CKEDITOR.replace('editor2', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            CKEDITOR.replace('editor3', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            CKEDITOR.replace('editor4', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            CKEDITOR.replace('editor5', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            CKEDITOR.replace('editor6', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            CKEDITOR.replace('editor7', {
                filebrowserBrowseUrl: roxyFileman,
                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                removeDialogTabs: 'link:upload;image:upload'
            });
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
</body>
</html>
