<style media="screen">
.sembunyi {
	display: none;
}
</style>
<?php
	$disabled = "disabled";

	$saklar = $this->uri->segment(3);

	if ($saklar == 'upload')
	{
		$data['outlet']	= $this->staf_model->get_outlet_single($this->uri->segment(4));

		$form_data = 'sembunyi';
		$form_upload = '';
	} else {
		$form_data = '';
		$form_upload = 'sembunyi';
	}

?>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>for all start activity</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active"><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<h3 style="text-align:center;">Welcome to</h3>
						<h2 style="text-align:center;">K-Admin Panel</h2>
					</div><!-- /.box-body -->
					<div class="box-footer">
						&nbsp;
					</div><!-- /.box-footer-->
				</div><!-- /.box -->

			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->
