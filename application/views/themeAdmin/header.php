<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Panel</title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/bootstrap/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?=base_url()?>font-awesome-4.6.3/css/font-awesome.min.css">

	<!-- Ionicons -->
	<link rel="stylesheet" href="<?=base_url()?>ionicons-2.0.1/css/ionicons.min.css">

	<!-- daterange picker -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/plugins/daterangepicker/daterangepicker-bs3.css">

	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/plugins/datepicker/datepicker3.css">

	<!-- Bootstrap time Picker -->
  	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/plugins/timepicker/bootstrap-timepicker.min.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/plugins/select2/select2.min.css">

	<!-- DataTables -->
  	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/plugins/datatables/dataTables.bootstrap.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/dist/css/AdminLTE.min.css">

	<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?=base_url()?>application/views/themeAdmin/dist/css/skins/_all-skins.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Custom style -->
	<style>
	.force-uppercase {
		text-transform: uppercase;
	}
	.form-control-plaintext {
		border: 0px;
	}
	</style>
</head>
<!-- onLoad="pop_up();" -->
<body class="hold-transition skin-yellow-light sidebar-mini" style="background-color: #f9fafc;">
	<!-- Site wrapper -->
	<div class="wrapper">
